#!/bin/bash -e

BUILD_BASE_DIR=/home/tomtranone/sense/system-utils
BINARIES_DIR=$BUILD_BASE_DIR/binaries

YOCTO_KERNEL_DIR=/home/tomtranone/sense/yocto_build/build/tmp/work-shared/intel-corei7-64/kernel-build-artifacts
YOCTO_IMAGE_DIR=/home/tomtranone/sense/yocto_build/build/tmp/deploy/images/intel-corei7-64

mkdir -p $BINARIES_DIR
mkdir -p $BINARIES_DIR/migration
mkdir -p $BINARIES_DIR/upgradekit

pushd .
cd cgutillx/cgoslx-x64-1.03.029/CgosDrv/Lx

make -C $YOCTO_KERNEL_DIR M=${PWD}
cp -av cgosdrv.ko $BINARIES_DIR/upgradekit/
popd

pushd .
cd cgutillx/cgoslx-x64-1.03.029/CgosLib/Lx
make
cp -av libcgos.so $BINARIES_DIR/upgradekit/
popd

pushd .
cd cgutillx/cgutlcmd/
if [ ! -L libcgos.so ]; then
   printf "No sym-link of libcgos.so, create symlink\n" 
   ln -s $BINARIES_DIR/upgradekit/libcgos.so
fi

make

cp -av cgutlcmd $BINARIES_DIR/upgradekit/
popd

make -C upgrade_apps


cp -av upgrade_apps/migration/* $BINARIES_DIR/migration
cp -v $YOCTO_IMAGE_DIR/systemd-bootx64.efi $BINARIES_DIR/migration/bootx64.efi
cp -v $YOCTO_IMAGE_DIR/bzImage $BINARIES_DIR/migration

cp -av upgrade_apps/blob_parse/blob_parse $BINARIES_DIR//upgradekit
cp -av upgrade_apps/gen_img/gen_img $BINARIES_DIR/upgradekit
cp -av upgrade_apps/sbc_upgrade/sbc_upgrade $BINARIES_DIR/upgradekit
cp -av upgrade_apps/libupgrade/libupgrade.so $BINARIES_DIR/upgradekit
cp -av upgrade_apps/scripts/*.sh $BINARIES_DIR/upgradekit
cp -av upgrade_apps/scripts/*.service $BINARIES_DIR/upgradekit

