#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <syslog.h>
#include <errno.h>

#define UPGRADE_FLAG    "/data/.upgrade/.upg_avail"

int main(void)
{
   int fd = -1;
   int err = 0;
   char filename[128];
   char cmd[512]="";
   struct stat st;
   size_t len = 0;
   size_t read_bytes = 0;

   err = system("fstrim /data; mkdir -p /data/.upgrade/");

   while(1) {
      if(stat(UPGRADE_FLAG, &st)) {
         /* Sleep 5 second if no upgrade available */
         usleep(5000000);
      } else {
         syslog(LOG_INFO, "[sbc_upgrade]: Upgrade available!");
         break;
      }
   }

   len = st.st_size;
   if(len == 0) {
      syslog(LOG_ERR, "[sbc_upgrade]: upgrade flag size 0 - ignore");
      err = system("rm -fr "UPGRADE_FLAG);
      /* Exit application because systemd will restart the service */
      exit(1);
   }
   fd = open(UPGRADE_FLAG, O_RDONLY);
   if(fd < 0) {
      syslog(LOG_ERR, "[sbc_upgrade]: Failed to open [%s]", UPGRADE_FLAG);
      err = system("rm -fr "UPGRADE_FLAG);
      /* Exit application because systemd will restart the service */
      exit(1);
   }
   read_bytes = read(fd, filename, len);
   if(read_bytes != len) {
      syslog(LOG_ERR, "[sbc_upgrade]: Read mismatch, read(%ld), expected(%ld)", read_bytes, len);
      err = system("rm -fr "UPGRADE_FLAG);
      close(fd);
      exit(1);
   }
   filename[len-1]='\0';
   close(fd);

   syslog(LOG_INFO, "[sbc_upgrade]: Extracting upgrade file [%s]", filename);

   sprintf(cmd, "tar xmjf /data/.upgrade/%s --warning=no-timestamp -C /data/.upgrade", filename);
   syslog(LOG_INFO, "[sbc_upgrade]: Executing cmd [%s]", cmd);
   err = system(cmd);
   if(err) {
      syslog(LOG_INFO, "[sbc_upgrade]: Extracting warning ...");
   }

   sprintf(cmd, "rm -fr /data/.upgrade/%s ", filename);
   syslog(LOG_INFO, "[sbc_upgrade]: Executing cmd [%s]", cmd);
   //err = system(cmd);

   err = system("/data/.upgrade/sw_upgrade.sh");
   if (err) {
      syslog(LOG_ERR, "[sbc_upgrade]: Error occurred during upgrade, check /var/log/messages");
      //err = system("rm -fr /data/.upgrade/*.bin; rm -fr "UPGRADE_FLAG);
      exit(1);
   }
   return 0;
}
