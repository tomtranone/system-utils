#!/bin/bash

function usage()
{
   printf "[MIGRATION]: usage: ./migrate.sh HOST=<IP Address of host> DEVICE=<IP Address of Device\n\n"
   exit 1
}

# Store the user password so we reduce the number time entering it.
while true; do
   printf "Password: "
   read -s password
   printf "\n"
   echo $password | sudo -S -k touch /var/local/test_file.txt
   #printf "result = %d\n" $?
   if [ $? != 0 ]; then
      printf "Wrong password\n"
   else
      printf "Password verified\n"
      echo $password | sudo -S -k rm -f /nfs/test_file.txt
      break
   fi
   sleep 1s
done

###############################################################
#  Function: check_params
###############################################################
check_params()
{
   printf "\n[MIGRATION]: Checking parameters ...\n"

   if [[ "$1" == "DEVICE"* ]]; then
      DEVICE_IP=`echo $1 | sed 's/DEVICE=//g'`
   fi

   if [[ "$2" == "DEVICE"* ]]; then
      DEVICE_IP=`echo $2 | sed 's/DEVICE=//g'`
   fi

   if [[ "$3" == "DEVICE"* ]]; then
      DEVICE_IP=`echo $3 | sed 's/DEVICE=//g'`
   fi

   if [ "$DEVICE_IP" = "" ]; then
      usage
      check_params_result=1
   fi

   if [[ "$1" == "HOST"* ]]; then
      HOST_IP=`echo $1 | sed 's/HOST=//g'`
   fi

   if [[ "$2" == "IP"* ]]; then
      HOST_IP=`echo $2 | sed 's/HOST=//g'`
   fi

   if [[ "$3" == "IP"* ]]; then
      HOST_IP=`echo $3 | sed 's/HOST=//g'`
   fi

   if [ "$HOST_IP" = "" ]; then
      usage
      check_params_result=1
   fi

   STATIC_IP=0

   if [[ "$1" == "-ipstatic" ]]; then
      STATIC_IP=1
   fi

   if [[ "$2" == "-ipstatic" ]]; then
      STATIC_IP=1
   fi

   if [[ "$3" == "-ipstatic" ]]; then
      STATIC_IP=1
   fi

   printf "[MIGRATION]: Host IP Address: %s\n" $HOST_IP
   printf "[MIGRATION]: Device IP Address: %s\n" $DEVICE_IP
   printf "[MIGRATION]: STATIC IP: %d\n" $STATIC_IP

   while true; do
      printf "Is this correct? (y/n) "
      read response

      if [ "$response" = "y" ]; then
         break;
      elif [ "$response" = "n" ]; then
         printf "Exiting ...\n"
         exit 1
      else
         printf "Incorrect response\n"
      fi
   done

   UBUNTU_VERSION=`lsb_release -d | grep "Description:" | awk {' print $3 '}`

   if [ "$UBUNTU_VERSION" == "18.04.4" ]; then

      printf "[MIGRATION]: Verifying HOST IP address ...\n"
      VERIFY=`ifconfig | grep "$HOST_IP" | awk {' print $2 '}`

      if [ "$VERIFY" != "$HOST_IP" ]; then
         printf "[MIGRATION]: Please check your network interface IP address - doesn't match interface\n"
         check_params_result=1
         exit 1
      fi
   fi

   if [ "$UBUNTU_VERSION" == "16.04.4" ]; then
      printf "[MIGRATION]: Verifying HOST IP address ...\n"
      VERIFY=`ifconfig | grep "$HOST_IP" | awk {' print $2 '} | sed 's/addr://g'`

      if [ "$VERIFY" != "$HOST_IP" ]; then
         printf "[MIGRATION]: Please check your network interface IP address - doesn't match interface\n"
         check_params_result=1
         exit 1
      fi
   fi

   printf "[MIGRATION]: HOST IP: %s - OK\n" $HOST_IP

   ping $DEVICE_IP -c 5
   if [ $? != 0 ]; then
      printf "[MIGRATION]: Cannot access device with ip [%s]\n" $DEVICE_IP
      usage
      check_params_result=1
   fi

   check_params_result=0
   if [ $STATIC_IP == 1 ]; then
      printf "\n[MIGRATION]: Contact device @%s - OK\n" $DEVICE_IP
   else
      printf "\n[MIGRATION]: Contact device @%s [static ip] - OK\n" $DEVICE_IP
   fi

   printf "\n[MIGRATION]: Checking parameters ... done\n"
}

###############################################################
#  Function: setup_nfs_server
###############################################################
function setup_nfs_server()
{
   printf "\n[MIGRATION]: Setting up NFS server ...\n"

   # Check to see if NFS Kernel Server is installed
   printf "Checking for nfs-kernel-server package ...\n"
   nfs_server_install_status=`dpkg -s nfs-kernel-server | grep "Status" | awk {' print $2 '}`

   if [ "$nfs_server_install_status" != "install" ]; then
      printf "====> nfs-kernel-server install now\n"
      echo $password | sudo -S apt install -y nfs-kernel-server

      # Interestingly, nfs-kernel-server will only create /etc/exports during the first install
      if [ ! -f /etc/exports ]; then
         echo $password | sudo -S -k touch /etc/exports
         echo $password | sudo -S -k chmod 766 /etc/exports
         # printf "No /etc/exports file - please check nfs-kernel-server again\n"
      fi

      while true; do
         sleep 2s
         systemctl restart nfs-kernel-server
         if [ $? == 0 ]; then
            break
         fi
      done

   fi


   EXPORTS=`cat /etc/exports | grep "/nfs/root"`
   if [ "$EXPORTS" = "" ]; then
      # Allow all IP(s) to mount to this NFS mount point
      # Interestingly, nfs-kernel-server will only create /etc/exports during the first install
	  printf "\n====> Adding exported directory ...\n"
      if [ ! -f /etc/exports ]; then
         echo $password | sudo -S -k touch /etc/exports

         # printf "No /etc/exports file - please check nfs-kernel-server again\n"
      fi

      echo $password | sudo -S -k chmod 766 /etc/exports

      printf "[MIGRATION]: Allowing clients to mount here /nfs/root\n"
      echo $password | sudo -S echo -e "/nfs/root *(rw,sync,fsid=0,crossmnt,no_subtree_check)" >> /etc/exports
	  if [ $? != 0 ]; then
 		 printf "ERROR: Setting up exported directory \n"
		 exit 1
      fi
      printf "/nfs/root *(rw,sync,fsid=0,crossmnt,no_subtree_check) added to /etc/exports\n"
   fi


   if [ ! -f nfs.tar.bz2 ]; then
      printf "[MIGRATION]: Unable to locate nfs.tar.bz2\n"
      exit 1
   fi

   echo $password | sudo -S mkdir -p /nfs
   if [ $? != 0 ]; then
      printf "[MIGRATION]: Cannot create /nfs for NFS mount point -wrong password?\n"
      exit 1
   fi

   if [ -d /nfs/root ]; then
      printf "[MIGRATION]: WARNING: /nfs/root exists - Moving to /nfs/BACKUP? (Y/N): "

      while true; do
         read answer
         
         if [ "$answer" == "N" ] || [ "$answer" == "n" ]; then
            printf "[MIGRATION]: Discontinue process!\n"
            exit 1
         fi
         if [ "$answer" == "Y" ] || [ "$answer" == "y" ]; then
            printf "[MIGRATION]: Moving previous /nfs/root to /nfs/BACKUP ...\n"
            count=0
            # if directory already exists from previous run(s), it will store the directory
            # to the next index
            while true; do
               if [ ! -d /nfs/BACKUP$count ]; then
                  break
               fi
               ((count++))
            done
            echo $password | sudo -S mv -v /nfs/root /nfs/BACKUP$count
            break
         fi
         if [ "$answer" != "Y" ] && [ "$answer" != "y" ] && [ "$answer" == "N" ] && [ "$answer" == "n" ]; then
            printf "[MIGRATION]: Inappropriate response\n"
            continue
         fi
      done
      
   fi

   echo $password | sudo -S cp -av nfs.tar.bz2 /nfs/
   if [ $? != 0 ]; then
      printf "[MIGRATION]: Failed to copy NFS filesystem to /nfs\n"
      exit 1
   fi

   pushd .

   cd /nfs

   printf "\n[MIGRATION]: Please wait ... setting up NFS\n"
   echo $password | sudo -S tar -xjf nfs.tar.bz2
   if [ $? != 0 ]; then
      printf "[MIGRATION]: Failed to extract NFS filesystem\n"
      exit 1
   fi

   echo $password | sudo -S mv -v tmp root
   if [ $? != 0 ]; then
      printf "[MIGRATION]: Failed to rename 'tmp' to 'root'\n"
      exit 1
   fi

   echo $password | sudo -S chmod -R 777 root

   popd

   # /nfs/root	192.168.2.3(rw,sync,fsid=0,crossmnt,no_subtree_check)
   echo $password | sudo -S exportfs -a
   if [ $? != 0 ]; then
      printf "[MIGRATION]: Cannot export the /nfs/root directory for NFS mount -please check NFS setup\n"
      exit 1
   fi

   printf "\n[MIGRATION]: Setting up NFS server [$HOST_IP]... done\n"
}

###############################################################
#  Function: save_user_data
###############################################################
function save_user_data()
{
   printf "\n[MIGRATION]: Saving user data to NFS ...\n"

   # Generate the save_user_data.sh script to be run on the SBC.
   echo -e "#!/bin/bash

# Check against running on new FLASH layout
if [ -f /boot/bzImage0 ]; then
   printf \"[SBC]: Already has new FLASH layout -- no need to continue\"
   exit 1
fi

# Saving previous system data
printf \"[SBC]: Saving user data to NFS ...\"
mkdir -p local_mnt
mount /dev/sda2 local_mnt
pushd .
cd local_mnt/home/root
tar cvjf sense.bins.tar.bz2 sense/bins/*
tar cvjf sense.configs.tar.bz2 sense/configs/*

mkdir -p /data/
mkdir -p /data/userdata/
cp *.bz2 /data/userdata/

popd
umount local_mnt
printf \"[SBC]: Saving user data done\"
" > save_user_data.sh

   chmod 777 save_user_data.sh

   scp save_user_data.sh root@"$DEVICE_IP":

   ssh root@"$DEVICE_IP" /home/root/save_user_data.sh
   if [ $? != 0 ]; then
	printf "Error saving data\n"
	exit 1
   fi
   printf "\n[MIGRATION]: Data store at /nfs/root/data/userdata/\n"
   printf "[MIGRATION]: Saving user data to NFS ... done\n"
}

###############################################################
#  Function: prepare_sbc_for_nfs_boot
###############################################################
function prepare_sbc_for_nfs_boot()
{
   printf "\n[MIGRATION]: Preparing SBC for NFS boot ...\n"

   # Generate the boot_nfs.sh to be run on the SBC.  It configures for a NFS boot. 
   if [ $STATIC_IP == 1 ]; then

      echo -e "
      #!/bin/bash

      # Setup system for NFS boot
      if [ -f /boot/bzImage0 ]; then
         printf \"[SBC]: Already has new FLASH layout -- no need to continue\"
         exit 1
      fi

      printf \"[SBC]: Setting up SBC for NFS boot ...\"
      echo -e \"title NFS
      linux /bzImage
      options root=/dev/nfs nfsroot=$HOST_IP:/nfs/root,tcp,v3 ip=$DEVICE_IP::192.168.1.1:255.255.255.0::eth1:off rw reboot=efi raid=noautodetect i915.enable_guc_loading=1 rootwait console=tty0\" > /boot/loader/entries/nfs.conf

      echo -e \"default nfs
      timeout 1\" > /boot/loader/loader.conf

      " > boot_nfs.sh
   else
      echo -e "
      #!/bin/bash

      # Setup system for NFS boot
      if [ -f /boot/bzImage0 ]; then
         printf \"[SBC]: Already has new FLASH layout -- no need to continue\"
         exit 1
      fi

      printf \"[SBC]: Setting up SBC for NFS boot ...\"
      echo -e \"title NFS
      linux /bzImage
      options root=/dev/nfs nfsroot=$HOST_IP:/nfs/root,tcp,v3 ip=dhcp rw reboot=efi raid=noautodetect i915.enable_guc_loading=1 rootwait console=tty0\" > /boot/loader/entries/nfs.conf

      echo -e \"default nfs
      timeout 1\" > /boot/loader/loader.conf

      " > boot_nfs.sh
   fi

   chmod 777 boot_nfs.sh

   scp boot_nfs.sh root@"$DEVICE_IP":

   ssh root@"$DEVICE_IP" /home/root/boot_nfs.sh
   if [ $? == 1 ]; then
      printf "[MIGRATION]: Already has a new FLASH layout- no migration required\n"
      nfs_prep_result=1
   else
      nfs_prep_result=0
   fi
   printf "\n[MIGRATION]: Preparing SBC for NFS boot ... done\n"
}

###############################################################
#  Function: reboot_and_wait
###############################################################
function reboot_and_wait()
{
   printf "\n[MIGRATION]: Rebooting SBC system ... \n"
   printf "\n[MIGRATION]: If nothing is happening in 20 seconds, hit [ENTER]\n"
   ssh root@"$DEVICE_IP" /sbin/reboot
   
   printf "\n[MIGRATION]: Waiting for system to boot back up ... \n"
   # We need to give time for the SBC system to reboot
   sleep 20s
   echo $password | sudo -S systemctl restart nfs-kernel-server

   while true; do
      ping $DEVICE_IP -c 1
      if [ $? == 0 ]; then
         printf "[MIGRATION]: SBC came back up - yay!\n"
         break
      else
         printf "[MIGRATION]: SBC still booting ... sleep 10s\n"
         sleep 10s
      fi
   done
   # Make sure NFS is mounted
   sleep 5s

   printf "[MIGRATION]: SBC system rebooted successfully!\n"
}

###############################################################
#  Function: initiate_nfs_boot
###############################################################
function initiate_nfs_boot()
{
   printf "[MIGRATION]: Initiating NFS boot ...\n"

   ssh-keygen -f "/home/$USER/.ssh/known_hosts" -R "$DEVICE_IP"

   ssh root@"$DEVICE_IP" /usr/bin/killall sensesdk-mgr

   # Prepare the SBC for NFS boot
   prepare_sbc_for_nfs_boot
   if [ $nfs_prep_result == 1 ]; then
      printf "[MIGRATION]: Already has a new FLASH layout- no migration required\n"
      exit 1
   fi

   reboot_and_wait
}

###############################################################
#  Function: main
###############################################################
function main()
{
   # Clear screen
   clear
   ssh-keygen -f "/home/$USER/.ssh/known_hosts" -R "$DEVICE_IP"

   # Check whether the parameters are valid
   check_params $1 $2 $3
   if [ $check_params_result != 0 ]; then
      printf "[MIGRATION]: Error in parameters\n"
   fi

   # Setup NFS server and generate the NFS client parameters
   setup_nfs_server

   # Start the migration process on the SBC.
   initiate_nfs_boot

   ssh-keygen -f "/home/$USER/.ssh/known_hosts" -R "$DEVICE_IP"

   printf "[MIGRATION]: NFS server[$HOST_IP] is alive!\n"
   printf "[MIGRATION]: NFS client[$DEVICE_IP] is alive!\n"

   printf "\n[MIGRATION]:=================================\n"
   printf "[MIGRATION]: Communicating over NFS started ...\n"
   printf "\n[MIGRATION]:=================================\n"

   # Save the user data first
   save_user_data

   if [ ! -f /sbin/sgdisk ]; then
      printf "[MIGRATION]: You need to install sgdisk\n"
      exit 1
   fi

   # Place the required utilities/tools on NFS (/nfs/root/{bin,sbin})
   echo $password | sudo -S cp -v /sbin/sgdisk /nfs/root/sbin/
   echo $password | sudo -S cp -v /usr/bin/rsync /nfs/root/usr/bin/

   # Place migrate_flash_partition.sh script on NFS (/nfs/root/home/root/)
   echo $password | sudo -S cp -v migrate_flash_partition.sh /nfs/root/home/root/

   # Place all appropriate binaries on NFS (/nfs/root/home/root/)
   echo $password | sudo -S cp -v bootx64.efi /nfs/root/home/root/
   echo $password | sudo -S cp -v bzImage /nfs/root/home/root/
   echo $password | sudo -S cp -v rootfs.ext4 /nfs/root/home/root/
   echo $password | sudo -S cp -v sbc_upgrade /nfs/root/home/root/
   echo $password | sudo -S cp -v sbc_upgrade.service /nfs/root/home/root/
   echo $password | sudo -S cp -v sensesdk-mgr.service /nfs/root/home/root/

   while true; do
      printf "\nIRREVERSIBLE PROCESS.  Continue? (y/n) "
      read unreversable_process
      if [ "$unreversable_process" == "y" ]; then
         break;
      fi
      if [ "$unreversable_process" == "n" ]; then
         printf "[MIGRATION]: User stopped process\n"
         exit 1
      fi
   done

   # Run migrate_flash_partition.sh via ssh
   ssh root@"$DEVICE_IP" /home/root/migrate_flash_partition.sh ROOTFS=/home/root/rootfs.ext4 /dev/sda
   if [ $? != 0 ]; then
      printf "[MIGRATION]: migrate_flash_partition.sh failed\n"
      exit 1
   fi

   # After we are done migrating, we will reboot to the newly migrated system.
   reboot_and_wait

   printf "\n=================================\n"
   printf " SBC is running on new FLASH layout\n"
   printf "\n=================================\n"

   ssh-keygen -f "/home/$USER/.ssh/known_hosts" -R "$DEVICE_IP"
   
   # Enable sbc_upgrade service (if not already done so) auto-start enable
   ssh root@"$DEVICE_IP" systemctl enable sbc_upgrade
   if [ $? != 0 ]; then
      printf "[MIGRATION]: systemctl enable sbc_upgrade service failed\n"
      exit 1
   fi
   
   # Start sbc_upgrade service (if not already)
   ssh root@"$DEVICE_IP" systemctl start sbc_upgrade
   if [ $? != 0 ]; then
      printf "[MIGRATION]: systemctl start sbc_upgrade service failed\n"
      exit 1
   fi

   # Enable sensesdk-mgr service (if not already done so) auto-start enable
   ssh root@"$DEVICE_IP" systemctl enable sensesdk-mgr
   if [ $? != 0 ]; then
      printf "[MIGRATION]: systemctl enable sensesdk-mgr service failed\n"
      exit 1
   fi

   # Start sensesdk-mgr service (if not already)
   ssh root@"$DEVICE_IP" systemctl start sensesdk-mgr
   if [ $? != 0 ]; then
      printf "[MIGRATION]: systemctl start sensesdk-mgr service failed\n"
      exit 1
   fi
   printf "[MIGRATION]: Your data is saved under (/nfs/root/data/userdata)\n"
   printf "[MIGRATION]: Completed successfully\n"
}

main $1 $2 $3








