#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>

#include "fw_blob.h"


int main(int argc, char **argv)
{
   int c;
   opterr = 0;
   fw_blob_t blob;
   uint8_t *buf;
   int err = 0;
   char *bios_image_name = NULL;
   char *linux_image_name = NULL;
   char *rootfs_image_name = NULL;
   char *sdk_image_name = NULL;
   char *output = NULL;
   int fd = -1;
   size_t bios_len = 0;
   size_t linux_len = 0;
   size_t rootfs_len = 0;
   size_t sdk_len = 0;
   size_t written = 0;


   while((c = getopt (argc, argv, "b:l:r:s:o:")) != -1) {
      
      switch (c) {
         case 'o':
            output = optarg;
            break;
         case 'b':
            bios_image_name = optarg;
            break;
         case 'l':
            linux_image_name = optarg;
            break;
         case 'r':
            rootfs_image_name = optarg;
            break;
         case 's':
            sdk_image_name = optarg;
            break;
         case '?':
         if(optopt == 'l' || optopt == 'r' || optopt == 'o') {
            printf("Option -%c requires an argument.\n", optopt);
         } else if (isprint (optopt)) {
            printf("Unknown option `-%c'.\n", optopt);
         } else
            printf("Unknown option character `\\x%x'.\n",optopt);
         exit(1);

         default:
         exit(1);
         }
   }
   
   if(!output) {
      printf("Please provide the output name\n");
      exit(1);
   }
   printf("Packaging FW blob: [%s]\n", output);
   
   if(!bios_image_name && !linux_image_name && !rootfs_image_name && !sdk_image_name) {
      printf("No point to generate upgrade with no upgrade binaries\n");
      exit(1);
   }


   memset(&blob, 0, sizeof(blob));
   
   FW_BLOB_SIGN_PTR(&blob) = FW_BLOB_SIGNATURE;
   FW_BLOB_VERSION_PTR(&blob) = (FW_BLOB_VERSION_MAJOR<<16)|(FW_BLOB_VERSION_MINOR);
   
   if(bios_image_name) {
      bios_len = get_file_size(bios_image_name);
      printf("Process BIOS file: [%s]\n", bios_image_name);
      FW_BLOB_BIOS_CRC32_PTR(&blob) = get_crc32_file(bios_image_name, bios_len);
      FW_BLOB_BIOS_SIZE_PTR(&blob) = bios_len;
      FW_BLOB_FLAGS_PTR(&blob) |= FW_FLAG_BIOS;
   }

   if(linux_image_name) {
      linux_len = get_file_size(linux_image_name);
      printf("Process Linux file: [%s]\n", linux_image_name);
      FW_BLOB_LINUX_CRC32_PTR(&blob) = get_crc32_file(linux_image_name, ONE_MEG);
      FW_BLOB_LINUX_SIZE_PTR(&blob) = linux_len;
      FW_BLOB_FLAGS_PTR(&blob) |= FW_FLAG_LINUX;
   }

   if(rootfs_image_name) {
      rootfs_len = get_file_size(rootfs_image_name);
      printf("Process ROOTFS file: [%s]\n", rootfs_image_name);
      FW_BLOB_RFS_CRC32_PTR(&blob) = get_crc32_file(rootfs_image_name, ONE_MEG);
      FW_BLOB_RFS_SIZE_PTR(&blob) = rootfs_len;
      FW_BLOB_FLAGS_PTR(&blob) |= FW_FLAG_RFS;
   }

   if(sdk_image_name) {
      sdk_len = get_file_size(sdk_image_name);
      printf("Process SDK file: [%s]\n", sdk_image_name);
      FW_BLOB_SDK_CRC32_PTR(&blob) = get_crc32_file(sdk_image_name, sdk_len);
      FW_BLOB_SDK_SIZE_PTR(&blob) = sdk_len;
      FW_BLOB_FLAGS_PTR(&blob) |= FW_FLAG_SDK;
   }

   fd = open(output,O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR |S_IRGRP|S_IROTH);
   if(fd < 0) {
      printf("Cannot open output file [%s]\n", output);
      exit(1);
   }

   /* Write FW BLOB header */
   written = write(fd, &blob, sizeof(blob));
   if(written != sizeof(blob)) {
      printf("Write mismatch write(%lu), expected(%lu)\n", written, sizeof(blob));
      exit(1);
   }

   /* BIOS upgrade */
   if(bios_image_name && (FW_BLOB_FLAGS_PTR(&blob) & FW_FLAG_BIOS)) {
      err = get_file_buffer(bios_image_name, &buf);
      if(err != 0) {
         printf("Error reading file bios image\n");
         exit(1);
      }
      //printf("Writing linux size: 0x%x, %lu\n", (uint32_t)linux_len, linux_len);
      written = write(fd, buf, bios_len);
      if(written != linux_len) {
         printf("Write mismatch write(%lu), expected(%lu)\n", written, bios_len);
         exit(1);
      }
      free(buf);
   }

   /* Linux Kernel upgrade */
   if(linux_image_name && (FW_BLOB_FLAGS_PTR(&blob) & FW_FLAG_LINUX)) {
      err = get_file_buffer(linux_image_name, &buf);
      if(err != 0) {
         printf("Error reading file linux image\n");
         exit(1);
      }
      //printf("Writing linux size: 0x%x, %lu\n", (uint32_t)linux_len, linux_len);
      written = write(fd, buf, linux_len);
      if(written != linux_len) {
         printf("Write mismatch write(%lu), expected(%lu)\n", written, linux_len);
         exit(1);
      }
      free(buf);
   }

   /* Root filesystem upgrade */
   if(rootfs_image_name && (FW_BLOB_FLAGS_PTR(&blob) & FW_FLAG_RFS)) {
      err = get_file_buffer(rootfs_image_name, &buf);
      if(err != 0) {
         printf("Error reading file rootfs image\n");
         exit(1);
      }
      //printf("Writing rootfs size: 0x%x, %lu\n", (uint32_t)rootfs_len, rootfs_len);
      written = write(fd, buf, rootfs_len);
      if(written != rootfs_len) {
         printf("Write mismatch write(%lu), expected(%lu)\n", written, rootfs_len);
         exit(1);
      }
      free(buf);
   }

   /* SDK upgrade */
   if(sdk_image_name && (FW_BLOB_FLAGS_PTR(&blob) & FW_FLAG_SDK)) {

      err = get_file_buffer(sdk_image_name, &buf);
      if(err != 0) {
         printf("Error reading file sdk image\n");
         exit(1);
      }
      //printf("Writing sdk size: 0x%x, %lu\n", (uint32_t)sdk_len, sdk_len);
      written = write(fd, buf, sdk_len);
      if(written != sdk_len) {
         printf("Write mismatch write(%lu), expected(%lu)\n", written, sdk_len);
         exit(1);
      }
      free(buf);
   }

   close(fd);
   printf("FW blob generated!\n");
   print_blob_header(&blob);
   return 0;
}


