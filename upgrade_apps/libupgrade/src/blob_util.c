#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>

#include "crc.h"
#include "fw_blob.h"
   
size_t get_file_size(const char* filename)
{
   struct stat st;
   if(stat(filename, &st) != 0) {
      perror("Error ");
      return 0;
   }
   return st.st_size;   
}

int get_file_buffer(const char *filename, uint8_t **buf)
{
   int fd = 0;
   size_t len = 0;
   size_t read_bytes = 0;

   if(!filename || !buf) {
      printf("Invalid parameters!\n");
      return -1;
   }

   len = get_file_size(filename);
   *buf = (uint8_t *)malloc(len);

   fd = open(filename, O_RDONLY);
   if(fd < 0) {
      perror("Error ");
      free(*buf);
      return -1;
   }

   read_bytes = read(fd, *buf, len);
   if(read_bytes != len) {
      printf("Error read mismatch\n");
      free(*buf);
      *buf = NULL;
      return -1;
   }
   close(fd);
   return 0;
}

uint32_t get_crc32_file(char *filename, uint32_t len)
{
   int fd;
   uint32_t crc = 0;
   size_t read_bytes = 0;
   uint8_t *buf;

   
   if(!filename) {
      printf("No input file\n");
      return 0;
   }

   crc_init();

   printf("Filename: %s, len of calc: %d\n", filename, len);
   buf = (uint8_t *)malloc(len);
   if(!buf) {
      perror(" Fail to allocate memory ");
      return 0;
   }
   fd = open(filename, O_RDONLY);
   if(fd < 0) {
      perror("Error ");
      free(buf);
      return 0;
   }

   read_bytes = read(fd, buf, len);
   if(read_bytes != len) {
      perror("Error : ");
      printf("Length read mismatch, read(%lu), expected(%u)\n", read_bytes, len);
      close(fd);
      free(buf);
      return 0;
   }
   close(fd);

   printf("Calculating CRC32 for file [%s] ---> ", filename);
   crc = crc32(buf, len);
   free(buf);
   printf("[0x%x]\n",crc);
   return crc;
}

int blob_parse(const char *input, const char *output_dir)
{
   int err = 0;
   int fd=-1;
   fw_blob_t *blob;
   size_t len = 0;
   size_t read_bytes = 0;
   size_t written = 0;
   uint8_t *buf = NULL;
   uint8_t *tmp;
   char bios_outfile[1024]="";
   char lk_outfile[1024]="";
   char rfs_outfile[1024]="";
   char sdk_outfile[1024]="";

   if(!input) {
      printf("No input file\n");
      err = -1;
      goto err_parse;
   }
   if(!output_dir) {
      strcat(bios_outfile, "./");
      strcat(lk_outfile, "./");
      strcat(rfs_outfile, "./");
      strcat(sdk_outfile, "./");
      
   } else {
      strncat(bios_outfile, output_dir, strlen(output_dir)+1);
      strncat(lk_outfile, output_dir, strlen(output_dir)+1);
      strncat(rfs_outfile, output_dir, strlen(output_dir)+1);
      strncat(sdk_outfile, output_dir, strlen(output_dir)+1);
   }

   strncat(bios_outfile, "bios.bin", strlen("bios.bin"));
   strncat(lk_outfile, "lk.bin", strlen("lk.bin"));
   strncat(rfs_outfile, "rfs.bin", strlen("rfs.bin"));
   strncat(sdk_outfile, "sdk.bin", strlen("sdk.bin"));

   //printf("LK Outfile: [%s]\n", lk_outfile);
   //printf("RFS Outfile: [%s]\n", rfs_outfile);
   //printf("SDK Outfile: [%s]\n", sdk_outfile);
   
   fd = open(input, O_RDONLY);
   if(fd < 0) {
      perror("Error ");
      err = errno;
      goto err_parse;
   }
   
   len = get_file_size(input);
   buf = (uint8_t *)malloc(len);
   if(!buf) {
      perror("Error ");
      err = errno;
      goto err_parse;
   }
   //printf(" len of blob: %lu\n", len);
   
   read_bytes = read(fd, buf, len);
   if(read_bytes != len) {
      printf("Read mismatch in size\n");
      err = -2;
      goto err_parse;
   }
   close(fd);
   
   blob = (fw_blob_t *)buf;
   
   print_blob_header(blob);
   
   //printf("Buffer address : 0x%p\n", buf);
   tmp = buf + sizeof(fw_blob_t);

   /* Check for BIOS upgrade */
   if((FW_BLOB_BIOS_SIZE_PTR(blob) > 0) && (FW_BLOB_FLAGS_PTR(blob)&FW_FLAG_BIOS)) {
      fd = open(lk_outfile, O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR |S_IRGRP|S_IROTH);
      if(fd < 0) {
         perror("Error ");
         err = errno;
         goto err_parse;
      }
      written = write(fd, tmp, FW_BLOB_BIOS_SIZE_PTR(blob));
      if(written != FW_BLOB_BIOS_SIZE_PTR(blob)) {
         printf("Write mismatch of bios size\b");
         err = -3;
         goto err_parse;
      }
      close(fd);
      tmp += FW_BLOB_BIOS_SIZE_PTR(blob);
   } else {
      tmp += sizeof(FW_BLOB_BIOS_CRC32_PTR(blob)) + sizeof(FW_BLOB_BIOS_SIZE_PTR(blob));
   }

   /* Check for Linux kernel upgrade */
   if((FW_BLOB_LINUX_SIZE_PTR(blob) > 0) && (FW_BLOB_FLAGS_PTR(blob)&FW_FLAG_LINUX)) {
      fd = open(lk_outfile, O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR |S_IRGRP|S_IROTH);
      if(fd < 0) {
         perror("Error ");
         err = errno;
         goto err_parse;
      }
      written = write(fd, tmp, FW_BLOB_LINUX_SIZE_PTR(blob));
      if(written != FW_BLOB_LINUX_SIZE_PTR(blob)) {
         printf("Write mismatch of linux size\b");
         err = -3;
         goto err_parse;
      }
      close(fd);
      tmp += FW_BLOB_LINUX_SIZE_PTR(blob);
   } else {
      tmp += sizeof(FW_BLOB_LINUX_CRC32_PTR(blob)) + sizeof(FW_BLOB_LINUX_SIZE_PTR(blob));
   }

   /* Check for ROOTFS upgrade */
   if((FW_BLOB_RFS_SIZE_PTR(blob) > 0) && (FW_BLOB_FLAGS_PTR(blob)&FW_FLAG_RFS)) {
   
      fd = open(rfs_outfile,O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR |S_IRGRP|S_IROTH);
      if(fd < 0) {
         perror("Error ");
         err = errno;
         goto err_parse;
      }
      written = write(fd, tmp, FW_BLOB_RFS_SIZE_PTR(blob));
      if(written != FW_BLOB_RFS_SIZE_PTR(blob)) {
         printf("Write mismatch of rootfs size\b");
         err = -4;
         goto err_parse;
      }
      close(fd);
      tmp += FW_BLOB_RFS_SIZE_PTR(blob);
   } else {
      tmp += sizeof(FW_BLOB_RFS_CRC32_PTR(blob)) + sizeof(FW_BLOB_RFS_SIZE_PTR(blob));
   }

   /* Check for SDK upgrade */
   if((FW_BLOB_SDK_SIZE_PTR(blob) > 0) && (FW_BLOB_FLAGS_PTR(blob)&FW_FLAG_SDK)) {
   
      fd = open(sdk_outfile,O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR |S_IRGRP|S_IROTH);
      if(fd < 0) {
         perror("Error ");
         err = errno;
         goto err_parse;
      }
      written = write(fd, tmp, FW_BLOB_SDK_SIZE_PTR(blob));
      if(written != FW_BLOB_SDK_SIZE_PTR(blob)) {
         printf("Write mismatch of sdk size\b");
         err = -5;
         goto err_parse;
      }
      close(fd);
      tmp += FW_BLOB_SDK_SIZE_PTR(blob);
   } else {
      tmp += sizeof(FW_BLOB_SDK_CRC32_PTR(blob)) + sizeof(FW_BLOB_SDK_SIZE_PTR(blob));
   }

   fd = 0;

   if(FW_BLOB_FLAGS_PTR(blob) & FW_FLAG_BIOS) {
      if(get_crc32_file(bios_outfile, FW_BLOB_BIOS_SIZE_PTR(blob)) != FW_BLOB_BIOS_CRC32_PTR(blob)) {
         printf("BIOS CRC32 mismatch error\n");
         err = -6;
         goto err_parse;
      }
      printf("BIOS CRC32: GOOD\n");
   }

   if(FW_BLOB_FLAGS_PTR(blob) & FW_FLAG_LINUX) {
      if(get_crc32_file(lk_outfile, ONE_MEG) != FW_BLOB_LINUX_CRC32_PTR(blob)) {
         printf("Linux CRC32 mismatch error\n");
         err = -7;
         goto err_parse;
      }
      printf("Linux kernel CRC32: GOOD\n");
   }

   if(FW_BLOB_FLAGS_PTR(blob) & FW_FLAG_RFS) {
      if(get_crc32_file(rfs_outfile, ONE_MEG) != FW_BLOB_RFS_CRC32_PTR(blob)) {
         printf("ROOTFS CRC32 mismatch error\n");
         err = -8;
         goto err_parse;
      }
      printf("ROOTFS CRC32: GOOD\n");
   }

   if(FW_BLOB_FLAGS_PTR(blob) & FW_FLAG_SDK) {
      if(get_crc32_file(sdk_outfile, FW_BLOB_SDK_CRC32_PTR(blob)) != FW_BLOB_SDK_CRC32_PTR(blob)) {
         printf("SDK CRC32 mismatch error\n");
         err = -9;
         goto err_parse;
      }
      printf("SDK CRC32: GOOD\n");
   }

err_parse:
   if(fd) {
      close(fd);
   }
   if(buf) {
      free(buf);
   }
   return err;
}

void print_blob_header(fw_blob_t *blob)
{
   printf("BLOB HEADER:\n");
   printf("Signature      : 0x%08x\n", FW_BLOB_SIGN_PTR(blob));
   printf("Version        : %x.%x.%x.%x\n", (FW_BLOB_VERSION_PTR(blob)>>24)&0xff,
                                      (FW_BLOB_VERSION_PTR(blob)>>16)&0xff,
                                      (FW_BLOB_VERSION_PTR(blob)>>8)&0xff,
                                      FW_BLOB_VERSION_PTR(blob)&0xff);
   printf("Flags          : 0x%08x\n", FW_BLOB_FLAGS_PTR(blob));
   printf("BIOS CRC32     : 0x%08x\n", FW_BLOB_BIOS_CRC32_PTR(blob));
   printf("BIOS Size      : 0x%08x (%.2f MB) \n", FW_BLOB_BIOS_SIZE_PTR(blob),
                                                (float)((float)FW_BLOB_BIOS_SIZE_PTR(blob)/(float)(1024*1024)));
   printf("Linux CRC32    : 0x%08x\n", FW_BLOB_LINUX_CRC32_PTR(blob));
   printf("Linux Size     : 0x%08x (%.2f MB) \n", FW_BLOB_LINUX_SIZE_PTR(blob),
                                                (float)((float)FW_BLOB_LINUX_SIZE_PTR(blob)/(float)(1024*1024)));
   printf("RootFS CRC32   : 0x%08x\n", FW_BLOB_RFS_CRC32_PTR(blob));
   printf("RootFS Size    : 0x%08x (%.2f MB)\n", FW_BLOB_RFS_SIZE_PTR(blob),
                                               (float)((float)FW_BLOB_RFS_SIZE_PTR(blob)/(float)(1024*1024)));
   printf("SDK CRC32      : 0x%08x\n", FW_BLOB_SDK_CRC32_PTR(blob));
   printf("SDK Size       : 0x%08x (%.2f MB)\n", FW_BLOB_SDK_SIZE_PTR(blob),
                                               (float)((float)FW_BLOB_SDK_SIZE_PTR(blob)/(float)(1024*1024)));
}
