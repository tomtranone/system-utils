#ifndef __FW_BLOB_H__
#define __FW_BLOB_H__

#define FW_BLOB_SIGNATURE           0x1f2e3d4c

#define FW_BLOB_VERSION_MAJOR       0x0100
#define FW_BLOB_VERSION_MINOR       0x0000


#define ONE_MEG                     (1024*1024)

#define FW_FLAG_BIOS             0x01
#define FW_FLAG_LINUX            0x02
#define FW_FLAG_RFS              0x04
#define FW_FLAG_SDK              0x08


typedef struct _fw_blob_t
{
   uint32_t signature;           /* Firmware upgrade signature */
   uint32_t fw_blob_ver;         /* Firmware upgrade version */

   uint32_t upg_flags;           /* Firmware upgrade flags, i.e. component(s) */

   uint32_t bios_crc32;          /* BIOS image CRC32 */
   uint32_t bios_size;           /* BIOS image size */

   uint32_t linux_crc32;         /* Linux kernel image CRC32 */
   uint32_t linux_size;          /* Linux kernel image size */
   
   uint32_t rfs_crc32;           /* Root filesystem CRC32 */
   uint32_t rfs_size;            /* Root filesystem size */

   uint32_t sdk_crc32;           /* SDK (tarzipped) tarzipped */
   uint32_t sdk_size;            /* SDK size tarzipped */

} fw_blob_t;

#define FW_BLOB_SIGN(x)          ((x).signature)
#define FW_BLOB_VERSION(x)       ((x).fw_blob_ver)
#define FW_BLOB_FLAGS(x)         ((x).upg_flags)
#define FW_BLOB_BIOS_CRC32(x)    ((x).bios_crc32)
#define FW_BLOB_BIOS_SIZE(x)     ((x).bios_size)
#define FW_BLOB_LINUX_CRC32(x)   ((x).linux_crc32)
#define FW_BLOB_LINUX_SIZE(x)    ((x).linux_size)
#define FW_BLOB_RFS_CRC32(x)     ((x).rfs_crc32)
#define FW_BLOB_RFS_SIZE(x)      ((x).rfs_size)
#define FW_BLOB_SDK_CRC32(x)     ((x).sdk_crc32)
#define FW_BLOB_SDK_SIZE(x)      ((x).sdk_size)

#define FW_BLOB_SIGN_PTR(x)         ((x)->signature)
#define FW_BLOB_VERSION_PTR(x)      ((x)->fw_blob_ver)
#define FW_BLOB_FLAGS_PTR(x)        ((x)->upg_flags)
#define FW_BLOB_BIOS_CRC32_PTR(x)   ((x)->bios_crc32)
#define FW_BLOB_BIOS_SIZE_PTR(x)    ((x)->bios_size)
#define FW_BLOB_LINUX_CRC32_PTR(x)  ((x)->linux_crc32)
#define FW_BLOB_LINUX_SIZE_PTR(x)   ((x)->linux_size)
#define FW_BLOB_RFS_CRC32_PTR(x)    ((x)->rfs_crc32)
#define FW_BLOB_RFS_SIZE_PTR(x)     ((x)->rfs_size)
#define FW_BLOB_SDK_CRC32_PTR(x)    ((x)->sdk_crc32)
#define FW_BLOB_SDK_SIZE_PTR(x)     ((x)->sdk_size)

size_t get_file_size(const char* filename);
int get_file_buffer(const char *filename, uint8_t **buf);
uint32_t get_crc32_file(char *filename, uint32_t len);
int blob_parse(const char *input, const char *output_dir);
void print_blob_header(fw_blob_t *blob);

#endif /* __FW_BLOB_H__ */
