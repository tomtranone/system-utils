/*
 * Author: Tom Tran
 *
 * File: uart_isp.h
 *
 * Purpose: Structures and definitions for ISP programming.
 *
*/

#ifndef __UART_ISP_H__
#define __UART_ISP_H__

#define ISP_START                         "?\r\n"

#define ISP_SYNCRONIZED_RESPONSE          "Synchronized\r\n"
#define ISP_OK_RESPONSE                   "OK\r\n"
#define ISP_FIFTY_MHZ                     "50000\r\n"


#define ISP_CMD_UNLOCK_FLASH              "U" /* U <Unlock Code> Table 478 */
#define ISP_CMD_SET_BAUDRATE              "B" /*  Set Baud Rate B <Baud Rate> <stop bit> Table 479 */
#define ISP_CMD_ECHO                      "A" /* Echo A <setting> Table 480 */
#define ISP_CMD_WRITE_RAM                 "W" /* Write to RAM W <start address> <number of bytes> Table 481 */
#define ISP_CMD_READ_MEM                  "R" /* Read Memory R <address> <number of bytes> Table 482*/
#define ISP_CMD_PREP_SECT_WRITE           "P" /* Prepare sectors for write operation P <start sector number> <end sector number> Table 483 */
#define ISP_CMD_COPY_RAM_TO_FLASH         "C" /* Copy RAM to flash C <Flash address> <RAM address> <number of bytes> Table 484 */
#define ISP_CMD_GO                        "G" /* Go G <address> <Mode> Table 485 */
#define ISP_CMD_ERASE_SECT                "E" /* Erase sectors E <start sector number> <end sector number> Table 486 */
#define ISP_CMD_BLANK_CHECK               "I" /* Blank check sectors I <start sector number> <end sector number> Table 487 */
#define ISP_CMD_READ_PART_ID              "J" /*Read Part ID J Table 488 */
#define ISP_CMD_READ_BOOT_VERSION         "K" /* Read Boot code version K Table 490 */
#define ISP_CMD_COMPARE_ADDR              "M" /* Compare M <address1> <address2> <number of bytes> Table 491 */
#define ISP_CMD_READ_UID                  "N" /* ReadUID N Table 492 */
#define ISP_CMD_READ_CRC                  "S" /* Read CRC checksum S <address> <number of bytes> Table */


#define ARM_MODE                          "A"
#define THUMB_MODE                        "T"

typedef struct _isp_cmd_unlock_t
{
   uint32_t code;
} isp_cmd_unlock_t;

typedef struct _isp_cmd_baudrate_t
{
   uint32_t baudrate;
   uint32_t stop_bit;
} isp_cmd_baudrate_t;

typedef struct _isp_cmd_write_ram_t
{
   uint32_t start_addr;
   uint8_t *buffer;
   uint32_t length;
} isp_cmd_write_ram_t;

typedef struct _isp_cmd_read_mem_t
{
   uint32_t addr;
   uint32_t length;
   uint8_t *src;
} isp_cmd_read_mem_t;

typedef struct _isp_cmd_prepare_sect_t
{
   uint32_t start_sector;
   uint32_t end_sector;
} isp_cmd_prepare_sect_t;

typedef struct _isp_cmd_copy_ram_to_flash_t
{
   uint32_t flash_addr;
   uint32_t ram_addr;
   uint32_t length;
} isp_cmd_copy_ram_to_flash_t;

typedef struct _isp_cmd_go_t
{
   uint32_t addr;
   uint8_t *mode;
} isp_cmd_go_t;

typedef struct _isp_cmd_erase_sect_t
{
   uint32_t start_sector;
   uint32_t end_sector;
} isp_cmd_erase_sect_t;

typedef struct _isp_cmd_blank_check_t
{
   uint32_t start_sector;
   uint32_t end_sector;
} isp_cmd_blank_check_t;

typedef struct _isp_cmd_compare_t
{
   uint32_t addr1;
   uint32_t addr2;
} isp_cmd_compare_t;

typedef struct _isp_cmd_read_chksum_t
{
   uint32_t addr;
   uint32_t length;
} isp_cmd_read_chksum_t;


#define CMD_OK(x)    (((x)[0] == 0x30) && ((x)[1] == 0xd) && ((x)[2] == 0xa))

#endif
