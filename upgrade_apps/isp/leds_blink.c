#include <errno.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdint.h>
#include <ctype.h>
#include <sys/stat.h>

static int set_interface_attribs(int fd, int speed)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    tty.c_cflag |= (CLOCAL | CREAD);    /* ignore modem controls */
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

    /* setup for non-canonical mode */
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tty.c_oflag &= ~OPOST;

    /* fetch bytes as they become available */
    tty.c_cc[VMIN] = 0;
    tty.c_cc[VTIME] = 5;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        printf("Error from tcsetattr: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

static void set_mincount(int fd, int mcount)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error tcgetattr: %s\n", strerror(errno));
        return;
    }

    tty.c_cc[VMIN] = mcount ? 1 : 0;
    tty.c_cc[VTIME] = 5;        /* half second timer */

    if (tcsetattr(fd, TCSANOW, &tty) < 0)
        printf("Error tcsetattr: %s\n", strerror(errno));
}

static int write_response_to_uart(int fd, const char *response, size_t len)
{
   int wlen;
   int count = 0;
   
   /* simple output */
   
   while(count < len) {
      wlen = write(fd, response+count, 1);
      if (wlen != 1) {
         printf("Error from write: %d, %d\n", wlen, errno);
         return -1;
      }
      count++;
   }
   tcdrain(fd);    /* delay for output */
   return 0;
}

static void blink_upgrade_leds(int fd, uint8_t led_no, uint8_t led_state)
{
   int err = 0;
   uint8_t buf[10];

   /* Send the UPGRADE EXT LEDS cmd */
   buf[0] = 0x2;
   buf[1] = 3;
   buf[2] = led_no;
   buf[3] = led_state;
   buf[4] = 0x3;
   
   err = write_response_to_uart(fd, buf, 5);
   if(err) {
      printf("write_response_to_uart error: UART LEDS\n");
      err = -1;
   }

}

int main(int argc, char **argv)
{
   char *portname = "/dev/ttyS1";
   int c;
   uint8_t led_no = 0;
   uint8_t led_state = 0;

   printf("Stopping sensesdk-mgr ...\n");
   system("systemctl stop sensesdk-mgr");

   while((c = getopt (argc, argv, "l:s:")) != -1) {
      
      switch (c) {
         case 'l':
            led_no = atoi(optarg);
            break;
         case 's':
            led_state = atoi(optarg);
            break;
         case '?':
            if(optopt == 'l') {
               printf("Option -%c requires an argument.\n", optopt);
            } else if (isprint (optopt)) {
               printf("Unknown option `-%c'.\n", optopt);
            } else {
               printf("Unknown option character `\\x%x'.\n",optopt);
            }
            exit(1);

         default:
            exit(1);
         }
   }

   printf("led_no = %x\n", led_no);
   printf("led_state = %x\n", led_state);
   int fd = open(portname, O_RDWR | O_NOCTTY | O_SYNC);
   if (fd < 0) {
      printf("Error opening %s: %s\n", portname, strerror(errno));
      return -1;
   }
   /*baudrate 115200, 8 bits, no parity, 1 stop bit */
   set_interface_attribs(fd, B115200);
   //set_mincount(fd, 0);                /* set to pure timed read */

   tcflush(fd, TCIFLUSH);


   blink_upgrade_leds(fd, led_no, led_state);
   close(fd);
   return 0;
}
