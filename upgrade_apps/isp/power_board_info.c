#include <errno.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdint.h>
#include <ctype.h>
#include <sys/stat.h>

static int set_interface_attribs(int fd, int speed)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    tty.c_cflag |= (CLOCAL | CREAD);    /* ignore modem controls */
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

    /* setup for non-canonical mode */
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tty.c_oflag &= ~OPOST;

    /* fetch bytes as they become available */
    tty.c_cc[VMIN] = 0;
    tty.c_cc[VTIME] = 5;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        printf("Error from tcsetattr: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

static void set_mincount(int fd, int mcount)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error tcgetattr: %s\n", strerror(errno));
        return;
    }

    tty.c_cc[VMIN] = mcount ? 1 : 0;
    tty.c_cc[VTIME] = 5;        /* half second timer */

    if (tcsetattr(fd, TCSANOW, &tty) < 0)
        printf("Error tcsetattr: %s\n", strerror(errno));
}

static int write_response_to_uart(int fd, const char *response, size_t len)
{
   int wlen;
   int count = 0;
   
   /* simple output */
   
   while(count < len) {
      wlen = write(fd, response+count, 1);
      if (wlen != 1) {
         printf("Error from write: %d, %d\n", wlen, errno);
         return -1;
      }
      count++;
   }
   tcdrain(fd);    /* delay for output */
   return 0;
}


static int read_uart_len(int fd, char *response, int len)
{
   unsigned char *tmp;
   int totlen;
   int rdlen;

   memset(response, 0, sizeof(*response));
   tmp = response;
   totlen = 0;
   while(totlen < len) {
      rdlen = read(fd, tmp, 1);
      if (rdlen > 0) {
         totlen += rdlen;
         tmp += rdlen;
         response[totlen]='\0';
         //printf("Read total %d: \n", totlen);
         usleep(1000);

      } else if (rdlen < 0) {
         //printf("Error from read: %d: %s\n", rdlen, strerror(errno));
      } else {  /* rdlen == 0 */
         //printf("Timeout from read\n");
         return 0;
      }
      
   }
   return totlen;
}

static void get_version(int fd, uint8_t *major, uint8_t *minor, uint8_t *sub_minor)
{
   int err = 0;
   uint8_t buf[10];
   uint8_t start_byte = 0;
   uint8_t len_pkt = 0;
   uint8_t major_tmp = 0;
   uint8_t minor_tmp = 0;
   uint8_t sub_minor_tmp = 0;
   int timeout_count = 100;

   /* Send the CMD_VERSION cmd */
   buf[0] = 0x2;
   buf[1] = 3;
   buf[2] = 0x5; /* CMD_VERSION */
   buf[3] = 1;
   buf[4] = 0x3;
   
   err = write_response_to_uart(fd, buf, 5);
   if(err) {
      printf("write_response_to_uart error: CMD_VERSION\n");
      err = -1;
   }

   int len = 0;
   while(1) {
read_again:
      len = read_uart_len(fd, &start_byte, 1);
      if(len > 0) {
         if(start_byte == 0x2) {
            while(1) {
               //printf("get lenth pkt: timeout_count %d\n",timeout_count);
               len = read_uart_len(fd, &len_pkt, 1);
               if(len_pkt == 4) {
                  //printf("get lenth pkt=4\n");
                  len = 0;
                  while(len <= 0) {
                     //printf("get major \n");
                     len = read_uart_len(fd, &major_tmp, 1);
                     if(len == 0) {
                        timeout_count--;
                        if(timeout_count == 0) {
                           printf("timeout1\n");
                           goto got_version;
                        }
                     }
                  }
                  
                  len = 0;
                  while(len <= 0) {
                     //printf("get minor \n");
                     len = read_uart_len(fd, &minor_tmp, 1);
                     if(len == 0) {
                        timeout_count--;
                        if(timeout_count == 0) {
                           printf("timeout2\n");
                           goto got_version;
                        }
                     }
                  }
                  len = 0;
                  while(len <= 0) {
                     //printf("get sub_minor \n");
                     len = read_uart_len(fd, &sub_minor_tmp, 1);
                     if(len == 0) {
                        timeout_count--;
                        if(timeout_count == 0) {
                           printf("timeout3\n");
                           goto got_version;
                        }
                     }
                  }
                  goto got_version;
               } else {
                  timeout_count--;
                  if(timeout_count == 0) {
                     goto got_version; /* Nothing */
                  }
                  goto read_again;
               }
            }
          }
       } else {
          usleep(2000);
          timeout_count--;
          if(timeout_count == 0) {
             printf("This command <get_version> is not support by the power board firmware - timeout\n");
             break;
          }
       }
   }
got_version:

   *major = major_tmp;
   *minor = minor_tmp;
   *sub_minor = sub_minor_tmp;
   return;
}

static void get_mandrel_temp(int fd, int8_t *mandrel_temp)
{
   int err = 0;
   uint8_t buf[10];
   uint8_t start_byte = 0;
   uint8_t len_pkt = 0;
   int8_t temp = 0;
   int timeout_count = 100;

   /* Send the CMD_GET_MANDREL_TEMP cmd */
   buf[0] = 0x2;
   buf[1] = 3;
   buf[2] = 0xe; /* CMD_GET_MANDREL_TEMP */
   buf[3] = 1;
   buf[4] = 0x3;
   
   err = write_response_to_uart(fd, buf, 5);
   if(err) {
      printf("write_response_to_uart error: CMD_GET_MANDREL_TEMP\n");
      err = -1;
   }

   int len = 0;
   while(1) {
read_temp_again:
      len = read_uart_len(fd, &start_byte, 1);
      if(len > 0) {
         if(start_byte == 0x2) {
            while(1) {
               //printf("get lenth pkt: timeout_count %d\n",timeout_count);
               len = read_uart_len(fd, &len_pkt, 1);
               if(len_pkt == 2) {
                  //printf("get lenth pkt=4\n");
                  len = 0;
                  while(len <= 0) {
                     len = read_uart_len(fd, &temp, 1);
                     if(len == 0) {
                        timeout_count--;
                        if(timeout_count == 0) {
                           goto got_temp;
                        }
                     }
                  }
                  goto got_temp;
               } else {
                  timeout_count--;
                  if(timeout_count == 0) {
                     goto got_temp; /* Nothing */
                  }
                  goto read_temp_again;
               }
            }
          }
       } else {
          usleep(2000);
          timeout_count--;
          if(timeout_count == 0) {
             printf("This command <get_mandrel_temperature> is not support by the power board firmware - timeout\n");
             break;
          }
       }
   }
   
got_temp:

   *mandrel_temp = temp;
   return;
}

static void get_mandrel_hi_thresh(int fd, int8_t *hi_threshold)
{
   int err = 0;
   uint8_t buf[10];
   uint8_t start_byte = 0;
   uint8_t len_pkt = 0;
   int8_t thresh = 0;
   int len = 0;
   int timeout_count = 100;

   /* Send the CMD_GET_MANDREL_HI_THRESH cmd */
   buf[0] = 0x2;
   buf[1] = 3;
   buf[2] = 0xa; /* CMD_GET_MANDREL_HI_THRESH */
   buf[3] = 1;
   buf[4] = 0x3;
   
   err = write_response_to_uart(fd, buf, 5);
   if(err) {
      printf("write_response_to_uart error: CMD_GET_MANDREL_HI_THRESH\n");
      err = -1;
   }

   while(1) {
read_hi_thresh_again:
      len = read_uart_len(fd, &start_byte, 1);
      if(len > 0) {
         if(start_byte == 0x2) {
            while(1) {
               //printf("get lenth pkt: timeout_count %d\n",timeout_count);
               len = read_uart_len(fd, &len_pkt, 1);
               if(len_pkt == 2) {
                  //printf("get lenth pkt=4\n");
                  len = 0;
                  while(len <= 0) {
                     len = read_uart_len(fd, &thresh, 1);
                     if(len == 0) {
                        timeout_count--;
                        if(timeout_count == 0) {
                           goto got_hi_thresh;
                        }
                     }
                  }
                  goto got_hi_thresh;
               } else {
                  timeout_count--;
                  if(timeout_count == 0) {
                     goto got_hi_thresh; /* Nothing */
                  }
                  goto read_hi_thresh_again;
               }
            }
          }
       } else {
          usleep(2000);
          timeout_count--;
          if(timeout_count == 0) {
             printf("This command <get_high_mandrel_threshold> is not support by the power board firmware - timeout\n");
             break;
          }
       }
   }
got_hi_thresh:

   *hi_threshold = thresh;
   return;
}

static void get_mandrel_lo_thresh(int fd, int8_t *lo_threshold)
{
   int err = 0;
   uint8_t buf[10];
   uint8_t start_byte = 0;
   uint8_t len_pkt = 0;
   int8_t thresh = 0;
   int len = 0;
   int timeout_count = 100;

   /* Send the CMD_GET_MANDREL_LO_THRESH cmd */
   buf[0] = 0x2;
   buf[1] = 3;
   buf[2] = 0xb; /* CMD_GET_MANDREL_LO_THRESH */
   buf[3] = 1;
   buf[4] = 0x3;
   
   err = write_response_to_uart(fd, buf, 5);
   if(err) {
      printf("write_response_to_uart error: CMD_GET_MANDREL_LO_THRESH\n");
      err = -1;
   }

   while(1) {
read_lo_thresh_again:
      len = read_uart_len(fd, &start_byte, 1);
      if(len > 0) {
         if(start_byte == 0x2) {
            while(1) {
               //printf("get lenth pkt: timeout_count %d\n",timeout_count);
               len = read_uart_len(fd, &len_pkt, 1);
               if(len_pkt == 2) {
                  //printf("get lenth pkt=4\n");
                  len = 0;
                  while(len <= 0) {
                     len = read_uart_len(fd, &thresh, 1);
                     if(len == 0) {
                        timeout_count--;
                        if(timeout_count == 0) {
                           goto got_lo_thresh;
                        }
                     }
                  }
                  goto got_lo_thresh;
               } else {
                  timeout_count--;
                  if(timeout_count == 0) {
                     goto got_lo_thresh; /* Nothing */
                  }
                  goto read_lo_thresh_again;
               }
            }
          }
       } else {
          usleep(2000);
          timeout_count--;
          if(timeout_count == 0) {
             printf("This command <get_low_mandrel_threshold> is not support by the power board firmware - timeout\n");
             break;
          }
       }
   }
got_lo_thresh:

   *lo_threshold = thresh;
   return;
}

static void get_mandrel_cooling_thresh(int fd, int8_t *cooling_temp)
{
   int err = 0;
   uint8_t buf[10];
   uint8_t start_byte = 0;
   uint8_t len_pkt = 0;
   int8_t temp = 0;
   int len = 0;
   int timeout_count = 100;

   /* Send the CMD_GET_MANDREL_COOLING_THRESH cmd */
   buf[0] = 0x2;
   buf[1] = 3;
   buf[2] = 0xc; /* CMD_GET_MANDREL_COOLING_THRESH */
   buf[3] = 1;
   buf[4] = 0x3;
   
   err = write_response_to_uart(fd, buf, 5);
   if(err) {
      printf("write_response_to_uart error: CMD_GET_MANDREL_COOLING_THRESH\n");
      err = -1;
   }

   while(1) {
read_cooling_temp_again:
      len = read_uart_len(fd, &start_byte, 1);
      if(len > 0) {
         if(start_byte == 0x2) {
            while(1) {
               //printf("get lenth pkt: timeout_count %d\n",timeout_count);
               len = read_uart_len(fd, &len_pkt, 1);
               if(len_pkt == 2) {
                  //printf("get lenth pkt=4\n");
                  len = 0;
                  while(len <= 0) {
                     len = read_uart_len(fd, &temp, 1);
                     if(len == 0) {
                        timeout_count--;
                        if(timeout_count == 0) {
                           goto got_cooling_temp;
                        }
                     }
                  }
                  goto got_cooling_temp;
               } else {
                  timeout_count--;
                  if(timeout_count == 0) {
                     goto got_cooling_temp; /* Nothing */
                  }
                  goto read_cooling_temp_again;
               }
            }
          }
       } else {
          usleep(2000);
          timeout_count--;
          if(timeout_count == 0) {
             printf("This command <get_cooling_mandrel_threshold> is not support by the power board firmware t- timeout\n");
             break;
          }
       }
   }
got_cooling_temp:

   *cooling_temp = temp;
   return;
}

static void get_mandrel_heating_thresh(int fd, int8_t *heating_temp)
{
   int err = 0;
   uint8_t buf[10];
   uint8_t start_byte = 0;
   uint8_t len_pkt = 0;
   int8_t temp = 0;
   int len = 0;
   int timeout_count = 100;

   /* Send the CMD_GET_MANDREL_HEATING_THRESH cmd */
   buf[0] = 0x2;
   buf[1] = 3;
   buf[2] = 0xd; /* CMD_GET_MANDREL_HEATING_THRESH */
   buf[3] = 1;
   buf[4] = 0x3;
   
   err = write_response_to_uart(fd, buf, 5);
   if(err) {
      printf("write_response_to_uart error: CMD_GET_MANDREL_HEATING_THRESH\n");
      err = -1;
   }

   while(1) {
read_heating_temp_again:
      len = read_uart_len(fd, &start_byte, 1);
      if(len > 0) {
         if(start_byte == 0x2) {
            while(1) {
               //printf("get lenth pkt: timeout_count %d\n",timeout_count);
               len = read_uart_len(fd, &len_pkt, 1);
               if(len_pkt == 2) {
                  //printf("get lenth pkt=4\n");
                  len = 0;
                  while(len <= 0) {
                     len = read_uart_len(fd, &temp, 1);
                     if(len == 0) {
                        timeout_count--;
                        if(timeout_count == 0) {
                           goto got_heating_temp;
                        }
                     }
                  }
                  goto got_heating_temp;
               } else {
                  timeout_count--;
                  if(timeout_count == 0) {
                     goto got_heating_temp; /* Nothing */
                  }
                  goto read_heating_temp_again;
               }
            }
          }
       } else {
          usleep(2000);
          timeout_count--;
          if(timeout_count == 0) {
             printf("This command <get_heating_mandrel_threshold> is not support by the power board firmware - timeout\n");
             break;
          }
       }
   }
got_heating_temp:

   *heating_temp = temp;
   return;
}

int set_mandrel_hi_thresh(int fd, int8_t hi_thresh)
{
   int err = 0;
   int8_t buf[10];

   printf("Setting the Mandrel HI Temperature Threshold -> %d\n", hi_thresh);
   /* Send the CMD_GET_MANDREL_HI_THRESH cmd */
   buf[0] = 0x2;
   buf[1] = 3;
   buf[2] = 0x6; /* CMD_GET_MANDREL_HI_THRESH */
   buf[3] = hi_thresh;
   buf[4] = 0x3;
   
   err = write_response_to_uart(fd, buf, 5);
   if(err) {
      printf("write_response_to_uart error: CMD_GET_MANDREL_HI_THRESH\n");
      err = -1;
   }
   
   return err;
}

int set_mandrel_lo_thresh(int fd, int8_t lo_thresh)
{
   int err = 0;
   int8_t buf[10];

   printf("Setting the Mandrel LO Temperature Threshold -> %d, 0x%x\n", lo_thresh, (uint8_t)lo_thresh);
   /* Send the CMD_SET_MANDREL_LO_THRESH cmd */
   buf[0] = 0x2;
   buf[1] = 3;
   buf[2] = 0x7; /* CMD_SET_MANDREL_LO_THRESH */
   buf[3] = lo_thresh;
   buf[4] = 0x3;
   
   err = write_response_to_uart(fd, buf, 5);
   if(err) {
      printf("write_response_to_uart error: CMD_SET_MANDREL_LO_THRESH\n");
      err = -1;
   }
   
   return err;
}

int set_mandrel_cooling_thresh(int fd, int8_t cooling_temp)
{
   int err = 0;
   int8_t buf[10];

   printf("Setting the Mandrel Cooling Temperature Threshold -> %d\n", cooling_temp);
   /* Send the CMD_SET_MANDREL_COOLING_THRESH cmd */
   buf[0] = 0x2;
   buf[1] = 3;
   buf[2] = 0x8; /* CMD_SET_MANDREL_COOLING_THRESH */
   buf[3] = cooling_temp;
   buf[4] = 0x3;
   
   err = write_response_to_uart(fd, buf, 5);
   if(err) {
      printf("write_response_to_uart error: CMD_SET_MANDREL_COOLING_THRESH\n");
      err = -1;
   }
   
   return err;
}

int set_mandrel_heating_thresh(int fd, int8_t heating_temp)
{
   int err = 0;
   int8_t buf[10];

   printf("Setting the Mandrel Heating Temperature Threshold -> %d\n", heating_temp);
   /* Send the CMD_SET_MANDREL_HEATING_THRESH cmd */
   buf[0] = 0x2;
   buf[1] = 3;
   buf[2] = 0x9; /* CMD_SET_MANDREL_HEATING_THRESH */
   buf[3] = heating_temp;
   buf[4] = 0x3;
   
   err = write_response_to_uart(fd, buf, 5);
   if(err) {
      printf("write_response_to_uart error: CMD_SET_MANDREL_HEATING_THRESH\n");
      err = -1;
   }
   
   return err;
}

static void get_board_revision(int fd, uint8_t *board_revision)
{
   int err = 0;
   uint8_t buf[10];
   uint8_t start_byte = 0;
   uint8_t len_pkt = 0;
   uint8_t board_rev = 0;
   int len = 0;
   int timeout_count = 100;

   /* Send the CMD_GET_BOARD_REV cmd */
   buf[0] = 0x2;
   buf[1] = 3;
   buf[2] = 0xf; /* CMD_GET_BOARD_REV */
   buf[3] = 1;
   buf[4] = 0x3;

   err = write_response_to_uart(fd, buf, 5);
   if(err) {
      printf("write_response_to_uart error: CMD_GET_BOARD_REV\n");
      err = -1;
   }

   while(1) {
read_board_rev_again:
      len = read_uart_len(fd, &start_byte, 1);
      if(len > 0) {
         if(start_byte == 0x2) {
            while(1) {
               //printf("get lenth pkt: timeout_count %d\n",timeout_count);
               len = read_uart_len(fd, &len_pkt, 1);
               if(len_pkt == 2) {
                  //printf("get lenth pkt=4\n");
                  len = 0;
                  while(len <= 0) {
                     len = read_uart_len(fd, &board_rev, 1);
                     if(len == 0) {
                        timeout_count--;
                        if(timeout_count == 0) {
                           goto got_board_rev;
                        }
                     }
                  }
                  goto got_board_rev;
               } else {
                  timeout_count--;
                  if(timeout_count == 0) {
                     goto got_board_rev; /* Nothing */
                  }
                  goto read_board_rev_again;
               }
            }
          }
       } else {
          usleep(2000);
          timeout_count--;
          if(timeout_count == 0) {
             printf("This command <get_high_mandrel_threshold> is not support by the power board firmware - timeout\n");
             break;
          }
       }
   }
got_board_rev:

   *board_revision = board_rev;
   return;
}

void help(void)
{
   printf("\n");
   printf("Usage: pwr_board_info <option>\n");
   printf("<option>:\n");
   printf("\t-r , Just read back the all values\n");
   printf("\t-h <value> , Setting the Mandrel HIGH temperature threshold, (-55, 125), Default 65C\n");
   printf("\t-l <value> , Setting the Mandrel LOW temperature threshold, (-55, 125), Default -5C\n");
   printf("\t-c <value> , Setting the Mandrel Cooling temperature threshold, (-55, 125), Default 55C\n");
   printf("\t-w <value> , Setting the Mandrel Heating temperature threshold, (-55, 125), Default 0C\n");
   printf("\n");
}

int main(int argc, char **argv)
{
   char *portname = "/dev/ttyS1";
   int c;
   uint8_t start_byte = 0;
   uint8_t len_pkt = 0;
   uint8_t major = 0;
   uint8_t minor = 0;
   uint8_t sub_minor = 0;
   int8_t mandrel_temp = 0;
   int8_t hi_threshold = 65;
   int8_t lo_threshold = -5;
   int8_t cooling_temp = 55;
   int8_t heating_temp = 0;
   int timeout_count = 100;
   uint8_t readback_only=0;
   uint8_t board_rev = 0;

   printf("Stopping sensesdk-mgr ...\n");
   system("systemctl stop sensesdk-mgr");
   
   while((c = getopt (argc, argv, "h:l:c:w:r")) != -1) {
      
      switch (c) {
         case 'r':
            readback_only=1;
            break;
         case 'h':
            hi_threshold = atoi(optarg);
            break;
         case 'l':
            lo_threshold = atoi(optarg);
            break;
         case 'c':
            cooling_temp = atoi(optarg);
            break;
         case 'w':
            heating_temp = atoi(optarg);
            break;
         case '?':
            if(optopt == 'h' || optopt == 'l' || optopt == 'c' || optopt == 'h') {
               printf("Option -%c requires an argument.\n", optopt);
               help();
            } else if (isprint (optopt)) {
               printf("Unknown option `-%c'.\n", optopt);
            } else {
               printf("Unknown option character `\\x%x'.\n",optopt);
            }
            exit(1);

         default:
            exit(1);
         }
   }

   
   int fd = open(portname, O_RDWR | O_NOCTTY | O_SYNC);
   if (fd < 0) {
      printf("Error opening %s: %s\n", portname, strerror(errno));
      return -1;
   }
   /*baudrate 115200, 8 bits, no parity, 1 stop bit */
   set_interface_attribs(fd, B115200);
   //set_mincount(fd, 0);                /* set to pure timed read */

   tcflush(fd, TCIFLUSH);

   
   get_version(fd, &major, &minor, &sub_minor);

#if 0
   printf("HI Threshold: %d\n", hi_threshold);
   printf("LO Threshold: %d\n", lo_threshold);
   printf("Cooling Threshold: %d\n", cooling_temp);
   printf("Heating_temp Threshold: %d\n", heating_temp);
#endif

   if(!readback_only) {
      set_mandrel_hi_thresh(fd, hi_threshold);
      set_mandrel_lo_thresh(fd, lo_threshold);
      set_mandrel_cooling_thresh(fd, cooling_temp);
      set_mandrel_heating_thresh(fd, heating_temp);
      get_mandrel_temp(fd, &mandrel_temp);
   }

   get_mandrel_temp(fd, &mandrel_temp);

   hi_threshold=0;
   get_mandrel_hi_thresh(fd, &hi_threshold);

   lo_threshold=0;
   get_mandrel_lo_thresh(fd, &lo_threshold);

   cooling_temp=0;
   get_mandrel_cooling_thresh(fd, &cooling_temp);

   heating_temp=0;
   get_mandrel_heating_thresh(fd, &heating_temp);

   board_rev = 0;
   get_board_revision(fd, &board_rev);

   printf("Power board Info\n");
   printf("Power board revision    : %d.%d.%d\n", (board_rev>>2)&0x1, (board_rev>>1)&0x1, board_rev&0x1);
   printf("Firmware Version        : %d.%d.%d\n", major, minor, sub_minor);
   printf("Mandrel Temperature     : %dC\n", mandrel_temp);
   printf("Mandrel HI Threshold    : %dC\n", hi_threshold);
   printf("Mandrel LO Threshold    : %dC\n", lo_threshold);
   printf("Mandrel Cooling off Temp: %dC\n", cooling_temp);
   printf("Mandrel Heating up Temp : %dC\n", heating_temp);
   
   close(fd);
   return 0;
}
