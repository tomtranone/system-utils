//============================================================================
// Name        : Test1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
//#include <opencv2/core.hpp>
//#include <opencv2/core/base.hpp>
#include <iostream>
#include <fstream>
#include <istream>
#include <sstream>
#include <map>
//#include "opencv2/highgui.hpp"
//#include <opencv2/imgproc.hpp>
//#include <opencv2/core/persistence.hpp>
//#include <cnpy.h>

#include "HealthMonitor.h"
#include "ADCLookupTable.h"

using namespace std;

int testmain()
{
    auto file_logger = spdlog::basic_logger_mt("basic_logger", "logs/basic.txt");
    spdlog::set_default_logger(file_logger);

    spdlog::info("Welcome to spdlog!");
    spdlog::error("Some error message with arg: {}", 1);

    spdlog::warn("Easy padding in numbers like {:08d}", 12);
    spdlog::critical("Support for int: {0:d};  hex: {0:x};  oct: {0:o}; bin: {0:b}", 42);
    spdlog::info("Support for floats {:03.2f}", 1.23456);
    spdlog::info("Positional args are {1} {0}..", "too", "supported");
    spdlog::info("{:<30}", "left aligned");

    spdlog::set_level(spdlog::level::debug); // Set global log level to debug
    spdlog::debug("This message should be displayed..");

    // change log pattern
    spdlog::set_pattern("[%H:%M:%S %z] [%n] [%^---%L---%$] [thread %t] %v");

    // Compile time log levels
    // define SPDLOG_ACTIVE_LEVEL to desired level
    SPDLOG_TRACE("Some trace message with param {}", {});
    SPDLOG_DEBUG("Some debug message");

    // Set the default logger to file logger
}

#include <stdio.h>      /* printf */
#include <math.h>       /* fmod */
#include <iostream>
#include <SerialStream.h>
int main(int argc, char* argv[]) {

/*
    std::cout << sizeof(float) << std::endl;
    std::cout << sizeof(double) << std::endl;
*/

    /*
    std::string testName = "./cropMask.npy";
    cnpy::NpyArray testN = cnpy::npy_load(testName);
    char* testid = testN.data<char>();
    char buffer[16];
    memset(buffer, 0, 16);
    strncpy(buffer, testid, testN.word_size);
    std::cout << buffer << std::endl;
    std::vector<size_t> tshape = testN.shape;
    cout <<  tshape.data() << " " << testN.word_size << " " << testN.num_vals << " " << std::endl;
    std::cout << testid << std::endl;
    for ( int i = 0; i < testN.num_vals; ++i )
    {
        //buf = *testid++;
//        buf = (unitid[i] & 0xFF );
        std::cout << *testid++ << "- ";
    }
    std::cout << std::endl;
*/
//    std::string fpnName = "./fpnFrame-2_0005-4803-0050-1116.npz"; // "./Params_4phase_0005-4803-0050-0813_21140000.npz";
//    std::string fpnName = "./fpn-3-0000-0000-0000-0000_EBTALL-2.npz";
#if 0
    char buf;

    std::string fpnName = argv[1];
//    std::string fpnName = "./fpn-3-0008-0710-0116-1413.npz";
    //cropMask-0000-0000-0000-0000_EBTALL-2
    cnpy::npz_t fpnFrame = cnpy::npz_load(fpnName);

    std::cout << fpnFrame.size() << std::endl;

    char buffer[32];
    memset(buffer, 0, 32);

    for (const auto &pair : fpnFrame) {
        std::cout << pair.first << ": " ;
        cnpy::NpyArray fpnunitId = fpnFrame[pair.first];
        std::vector<size_t> shape = fpnunitId.shape;

        if ( shape.size() == 0 && fpnunitId.num_vals == 1 )
        {
            char* unitid = fpnunitId.data<char>();
            if ( fpnunitId.word_size > 1 )
            {
                strncpy(buffer, unitid, fpnunitId.word_size);
                std::cout << "Value read is " << buffer ;
            }
            else
                std::cout << "Value read is " << (uint16_t) unitid[0] ;
        }

        if ( shape.size() != 0 && fpnunitId.word_size == sizeof(double) )
        {
            double* data = fpnunitId.data<double>();
            std::cout << shape.size() << ":" << fpnunitId.num_vals;
        }

        std::cout << std::endl;

    }

    return 1;
//    cnpy::NpyArray fpnFrameData = fpnFrame["FPN"];
    cnpy::NpyArray fpnunitId = fpnFrame["unit_id"];
    cnpy::NpyArray fpnVersion = fpnFrame["version"];
    cnpy::NpyArray passfail = fpnFrame["pass_fail"];
    cnpy::NpyArray timestamp = fpnFrame["timestamp"];

/*
    cnpy::NpyArray fpnFrame = cnpy::npz_load(fpnName, "fpnFrame");
    cnpy::NpyArray fpnunitId = cnpy::npz_load(fpnName, "unit_id");
    cnpy::NpyArray fpnVersion = cnpy::npz_load(fpnName, "version");
*/

    char* unitid = fpnunitId.data<char>();
    std::vector<size_t> shape = fpnunitId.shape;
    cout <<  shape.size() << " " << fpnunitId.word_size << " " << fpnunitId.num_vals << " " << std::endl;


    strncpy(buffer, unitid, fpnunitId.word_size);
    std::cout << "Unit id is " << buffer << std::endl;

    memset(buffer, 0, 32);
    char* fpnVer = fpnVersion.data<char>();

    strncpy(buffer, fpnVer, fpnVersion.word_size);
    std::cout << "FPN version is " << buffer << std::endl;

    memset(buffer, 0, 32);
    char* timestampbuf = timestamp.data<char>();
    strncpy(buffer, timestampbuf, timestamp.word_size);
    std::cout << "Timestamp is " << buffer << std::endl;

    char* pf = passfail.data<char>();
    std::cout << "pass fail value is " << (uint16_t)pf[0] << endl;

    return 0;
#endif

	SPHealthMonitor* aHMon = 0;
/*	unsigned short adc[8] = { 0x123, 0x123, 0x123, 0x123, 0x123, 0x123, 0x123, 0x123 };

	unsigned char hb = (unsigned char) (adc[0] >> 8) & 0xFF;
	unsigned char lb = (unsigned char) adc[0] & 0xFF;
	cout << adc[0] << " " << hb << " " << lb << endl;

	std::map<unsigned short int, int> tempLutmap;
	//tempLutmap.insert[g_LUT[0].ADC] = g_LUT[0].temp;
	for (int i = 0; i < sizeof(g_LUT)/sizeof(ADCLut); ++i )
		tempLutmap.insert( std::pair<unsigned short int, int>(g_LUT[i].ADC, g_LUT[i].temp) );*/

/*
	auto tp = tempLutmap.find(10);
	std::cout << tp->second << std::endl;
	tp = tempLutmap.find(870);
	std::cout << tp->second << std::endl;
*/
/*	for (auto& x: tempLutmap)
	{
	    std::cout << x.first << ": " << x.second << '\n';
	}*/
	//std::cout << g_LUT[0].ADC << g_LUT[0].temp << std::endl;

	bool breturn = false;
//	testmain();

	breturn = SPHealthMonitor::CreateHealthMonitor(aHMon);
	if ( breturn == false)
	{
		cout << "Failed to create health monitor instance" << endl;
		return 0;
	}

	cout <<"Created health monitor" << endl;
	std::string port = "/dev/ttyS1";
   int8_t hi_thresh=65;
   int8_t lo_thresh=-1;
   int8_t cooling_thresh=56;
   int8_t heating_thresh=1;
	aHMon->Init(port, 195, hi_thresh, lo_thresh, cooling_thresh, heating_thresh);
	cout <<"Initialized health monitor" << endl;

	int id = 1;
	SPHeathDataInterface* dataInterfacePtr = new SPHeathDataInterface(id);
	aHMon->RegisterCallback(dataInterfacePtr);

	id = 2;
	dataInterfacePtr = new SPHeathDataInterface(id);
	aHMon->RegisterCallback(dataInterfacePtr);
	id = 3;
	dataInterfacePtr = new SPHeathDataInterface(id);
	aHMon->RegisterCallback(dataInterfacePtr);


	while(1)
	{
		sleep(2);
        std::cout << " GAN FET1 temp:" << dataInterfacePtr->ganFET1Temp << std::endl;
        std::cout << " GAN FET2 temp:" << dataInterfacePtr->ganFET2Temp << std::endl;
        std::cout << " Mandrel_Temp:" << dataInterfacePtr->mandrelTemp << std::endl;
	}


	return 0;
}
