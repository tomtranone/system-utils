#include <errno.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdint.h>
#include <ctype.h>
#include <sys/stat.h>

static int set_interface_attribs(int fd, int speed)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    tty.c_cflag |= (CLOCAL | CREAD);    /* ignore modem controls */
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

    /* setup for non-canonical mode */
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tty.c_oflag &= ~OPOST;

    /* fetch bytes as they become available */
    tty.c_cc[VMIN] = 0;
    tty.c_cc[VTIME] = 5;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        printf("Error from tcsetattr: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

static void set_mincount(int fd, int mcount)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error tcgetattr: %s\n", strerror(errno));
        return;
    }

    tty.c_cc[VMIN] = mcount ? 1 : 0;
    tty.c_cc[VTIME] = 5;        /* half second timer */

    if (tcsetattr(fd, TCSANOW, &tty) < 0)
        printf("Error tcsetattr: %s\n", strerror(errno));
}

static int write_response_to_uart(int fd, const char *response, size_t len)
{
   int wlen;
   int count = 0;
   
   /* simple output */
   
   while(count < len) {
      wlen = write(fd, response+count, 1);
      if (wlen != 1) {
         printf("Error from write: %d, %d\n", wlen, errno);
         return -1;
      }
      count++;
   }
   tcdrain(fd);    /* delay for output */
   return 0;
}

static int read_uart_len(int fd, char *response, int len)
{
   unsigned char *tmp;
   int totlen;
   int rdlen;

   memset(response, 0, sizeof(*response));
   tmp = response;
   totlen = 0;
   while(totlen < len) {
      rdlen = read(fd, tmp, 1);
      if (rdlen > 0) {
         totlen += rdlen;
         tmp += rdlen;
         response[totlen]='\0';
         //printf("Read total %d: \n", totlen);
         usleep(1000);

      } else if (rdlen < 0) {
         //printf("Error from read: %d: %s\n", rdlen, strerror(errno));
      } else {  /* rdlen == 0 */
         //printf("Timeout from read\n");
         return 0;
      }
      
   }
   return totlen;
}

static void enter_serial_menu(int fd)
{
   int err = 0;
   uint8_t buf[10];

   /* Send the UPGRADE EXT LEDS cmd */
   buf[0] = 0x2;
   buf[1] = 3;
   buf[2] = 0x4;
   buf[3] = 0x4;
   buf[4] = 0x3;
   
   err = write_response_to_uart(fd, buf, 5);
   if(err) {
      printf("write_response_to_uart error: SERIAL MENU\n");
      err = -1;
   }

}

static size_t get_file_size(const char* filename)
{
   struct stat st;
   if(stat(filename, &st) != 0) {
      perror("Error ");
      exit(1);
   }
   return st.st_size;   
}

static int check_service_start(void)
{
   uint8_t buf[20];
   int err = system("systemctl is-active sensesdk-mgr > start.txt");

   int len = get_file_size("start.txt");

   int fd = open("start.txt", O_RDONLY);
   if(fd < 0) {
      perror("File error");
   }

   int read_count = read(fd, buf, len);
   if(read_count != len) {
      printf("read mismatch expect %d, received %d\n", len, read_count);
      err = 1;
      goto error;
   }

   if(strncmp(buf, "active", len-1) == 0) {
      printf("sensesdk-mgr service is started, disabling it!\n");
      system("systemctl stop sensesdk-mgr");
   } else {
      printf("sensesdk-mgr not started!\n");
   }
error:

   system("rm start.txt");
   if(fd > 0) close(fd);
   return err;
}

int main(int argc, char **argv)
{
   char *portname = "/dev/ttyS1";
   int read_count = 0;
   int prev_read_count = 0;
   int no_read_count = 0;
   int no_more = 0;

   printf("Stopping sensesdk-mgr ...\n");
   system("systemctl stop sensesdk-mgr");

   int fd = open(portname, O_RDWR | O_NOCTTY | O_SYNC);
   if (fd < 0) {
      printf("Error opening %s: %s\n", portname, strerror(errno));
      return -1;
   }
   /*baudrate 115200, 8 bits, no parity, 1 stop bit */
   set_interface_attribs(fd, B115200);
   //set_mincount(fd, 0);                /* set to pure timed read */

   check_service_start();
   tcflush(fd, TCIFLUSH);
   
   while(read_count < 100) {
      uint8_t tmp;
      read_count += read_uart_len(fd, &tmp, 1);
      if (read_count == 0) {
         no_read_count++;
         if(no_read_count == 300) {
            break;
         }
      }
      //printf("read_count = %d\n", read_count);
      if(prev_read_count == read_count) {
         no_more++;
         if(no_more == 20) {
            break;
         }
      }
      prev_read_count = read_count;
      //printf("prev_read_count = %d\n", prev_read_count);
      
   }

   enter_serial_menu(fd);
   close(fd);
   printf("FW is in serial menu mode - run minicom\n");
   return 0;
}
