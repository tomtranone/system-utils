/*
 * Author: Tom Tran
 *
 * File: uart_isp.c
 *
 * Purpose: Program the LPC1519 via RS422.
 *
*/

#include <errno.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdint.h>
#include <ctype.h>
#include <sys/stat.h>
//#include <syslog.h>

//#include "crc.h"

#include "uart_isp.h"

#define SECTOR_SIZE   4096


static void add_log(char *msg)
{
   char buf[1024];

   sprintf(buf,"logger \"[sbc_upgrade]: %s\"", msg);
   system(buf);
}

static int set_interface_attribs(int fd, int speed)
{
   char buf[100];
   struct termios tty;

   if (tcgetattr(fd, &tty) < 0) {
      sprintf(buf, "Error from tcgetattr: %s\n", strerror(errno));
      add_log(buf);
      return -1;
   }

   cfsetospeed(&tty, (speed_t)speed);
   cfsetispeed(&tty, (speed_t)speed);

   tty.c_cflag |= (CLOCAL | CREAD);    /* ignore modem controls */
   tty.c_cflag &= ~CSIZE;
   tty.c_cflag |= CS8;         /* 8-bit characters */
   tty.c_cflag &= ~PARENB;     /* no parity bit */
   tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
   tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

   /* setup for non-canonical mode */
   tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
   tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
   tty.c_oflag &= ~OPOST;

   /* fetch bytes as they become available */
   tty.c_cc[VMIN] = 0;
   tty.c_cc[VTIME] = 5;

   if (tcsetattr(fd, TCSANOW, &tty) != 0) {
      sprintf(buf, "Error from tcsetattr: %s\n", strerror(errno));
      add_log(buf);
      return -1;
   }
   return 0;
}

static void set_mincount(int fd, int mcount)
{
   char buf[100];
   struct termios tty;

   if (tcgetattr(fd, &tty) < 0) {
      sprintf(buf, "Error tcgetattr: %s\n", strerror(errno));
      add_log(buf);
      return;
   }

   tty.c_cc[VMIN] = mcount ? 1 : 0;
   tty.c_cc[VTIME] = 5;        /* half second timer */

   if (tcsetattr(fd, TCSANOW, &tty) < 0) {
      sprintf(buf, "Error tcsetattr: %s\n", strerror(errno));
      add_log(buf);
   }
}

static int read_response_from_uart(int fd, const char *response)
{
   unsigned char buf[80];
   unsigned char *tmp;
   int totlen;
   int rdlen;
   int error_count = 0;

   memset(buf, 0, sizeof(buf));
   tmp = buf;
   totlen = 0;
   while(1) {
      rdlen = read(fd, tmp, sizeof(buf) - 1);
      if (rdlen > 0) {
         totlen += rdlen;
         tmp += rdlen;
         buf[totlen]='\0';
         //printf("Read %d: \"%s\"\n", rdlen, buf);

      } else if (rdlen < 0) {
         char msg[100];
         sprintf(msg, "Error from read: %d: %s\n", rdlen, strerror(errno));
         add_log(msg);
      } else {  /* rdlen == 0 */
         //printf("Timeout from read error count: %d\n", error_count);
         error_count++;
         if(error_count == 100) {
            return -1;
         }
      }
      
      if(strncmp(buf, response, strlen(response)) == 0) {
         char msg[100];
         sprintf(msg, "Found response from UART ISP\n");
         add_log(msg);
         break;
      }
   }
   return 0;
}

static int read_uart_len(int fd, char *response, int len)
{
   unsigned char *tmp;
   int totlen;
   int rdlen;

   memset(response, 0, sizeof(*response));
   tmp = response;
   totlen = 0;
   while(totlen < len) {
      rdlen = read(fd, tmp, 1);
      if (rdlen > 0) {
         totlen += rdlen;
         tmp += rdlen;
         response[totlen]='\0';
         //printf("Read total %d: \n", totlen);
         usleep(1000);

      } else if (rdlen < 0) {
         //printf("Error from read: %d: %s\n", rdlen, strerror(errno));
      } else {  /* rdlen == 0 */
         //printf("Timeout from read\n");
         return 0;
      }
      
   }
   return totlen;
}

static int write_response_to_uart(int fd, const char *response, size_t len)
{
   int wlen;
   int count = 0;
   char msg[100];
   /* simple output */
   
   while(count < len) {
      wlen = write(fd, response+count, 1);
      if (wlen != 1) {
         sprintf(msg, "Error from write: %d, %d\n", wlen, errno);
         add_log(msg);
         return -1;
      }
      count++;
   }
   tcdrain(fd);    /* delay for output */
   return 0;
}


static int send_cmd_to_isp(int fd, const char *cmd, void *params)
{
   int err = 0;
   char msg[1024];
   unsigned char buf[8196]="";

   if(strcmp(cmd, ISP_CMD_UNLOCK_FLASH) == 0) {
      isp_cmd_unlock_t *lock = (isp_cmd_unlock_t *)params;
      sprintf(msg, "Unlock Flash : ");
      add_log(msg);
      sprintf(buf,"%s %d\r\n", ISP_CMD_UNLOCK_FLASH, lock->code);
      write_response_to_uart(fd, buf, strlen(buf));

      memset(buf, 0, sizeof(buf));
      int bytes_read = 0;
      while(bytes_read < 3) {
         bytes_read += read_uart_len(fd, buf+bytes_read, 1);
      }
      
#ifdef DEBUG
      for(int i=0; i<3; i++) {
         printf("0x%x ",buf[i]);
      }
      printf("\n");
#endif
      if(CMD_OK(buf)) {
         sprintf(msg, "successful\n");
      } else {
         sprintf(msg, "unsuccessful - error code %x\n", buf[0]);
         err = buf[0];
      }
      add_log(msg);
   } else if(strcmp(cmd, ISP_CMD_SET_BAUDRATE) == 0) {
      isp_cmd_baudrate_t *baud = (isp_cmd_baudrate_t  *)params;
      sprintf(msg, "Set baudrate : ");
      add_log(msg);

   } else if(strcmp(cmd, ISP_CMD_ECHO) == 0) {
      uint32_t *echo = (uint32_t *)params;
      sprintf(msg, "Echo : ");
      add_log(msg);
      sprintf(buf,"%s %d\r\n", ISP_CMD_ECHO, *echo);
      write_response_to_uart(fd, buf, strlen(buf));
      memset(buf, 0, sizeof(buf));
      int bytes_read = 0;
      while(bytes_read < 8) {
         bytes_read += read_uart_len(fd, buf+bytes_read, 1);
      }
      //read_uart_len(fd, buf, 8);
      //for(int i=0; i<8; i++) {
      //   printf("0x%x ",buf[i]);
      //}
      if(buf[0] == 'A' && buf[2] == '0') {
         sprintf(msg, "successful\n");
      } else {
         sprintf(msg, "unsuccessful - error code %x\n", buf[2]);
         err = buf[2];
      }
      add_log(msg);
   } else if(strcmp(cmd, ISP_CMD_WRITE_RAM) == 0) {
      isp_cmd_write_ram_t *wr_ram = (isp_cmd_write_ram_t *)params;

      sprintf(msg, "Write to RAM addr: 0x%x, length: %d : ", wr_ram->start_addr, wr_ram->length);
      add_log(msg);

      sprintf(buf,"%s %u %d\r\n", ISP_CMD_WRITE_RAM, wr_ram->start_addr, wr_ram->length);
      write_response_to_uart(fd, buf, strlen(buf));
      memset(buf, 0, sizeof(buf));
      int bytes_read = 0;
      while(bytes_read < 3) {
         bytes_read += read_uart_len(fd, buf+bytes_read, 1);
      }
      //read_uart_len(fd, buf, 3);
#ifdef DEBUG
      for(int i=0; i<3; i++) {
         printf("0x%x ",buf[i]);
      }
      printf("\n");
#endif
      if(CMD_OK(buf)) {
         sprintf(msg, "successful\n");
      } else {
         sprintf(msg, "unsuccessful - error code %x\n", buf[0]);
         err = buf[0];
         goto err_done;
      }
      add_log(msg);
      sprintf(msg, "Writing buffer to MCU RAM ...\n");
      add_log(msg);
      write_response_to_uart(fd, wr_ram->buffer, wr_ram->length);
      //write_response_to_uart(fd, "\r\n", 2);
      usleep(1000);

      /* Since there's no response let's read back from the memory */
#ifdef NO_WORKAROUND_FOR_CHIP_ERROR
      memset(buf, 0, sizeof(buf));
      read_uart_len(fd, buf, 4);
      for(int i=0; i<4; i++) {
         printf("0x%x ",buf[i]);
      }
      printf("\n");
      if((buf[0] == 'O') && (buf[1] == 'K') && (buf[2] == 0xd) && (buf[3] == 0xa)) {
         sprintf(msg, "dataWrite-to-RAM successful\n");
      } else {
         sprintf(msg, "dataWrite-to-RAM unsuccessful - error code %x\n", buf[0]);
         err = buf[0];
         goto err_done;
      }
      add_log(msg);
#endif

#ifdef VERIFY_WRITE_TO_RAM
      /* Workaround - just read back the RAM location to see if the data is correct */
      isp_cmd_read_mem_t read_back;
      memset(&read_back, 0, sizeof(read_back));
      
      read_back.addr = wr_ram->start_addr;
      read_back.length = wr_ram->length;
      read_back.src = wr_ram->buffer;
      
      err = send_cmd_to_isp(fd, ISP_CMD_READ_MEM, &read_back);
#endif

   } else if(strcmp(cmd, ISP_CMD_READ_MEM) == 0) {
      isp_cmd_read_mem_t *rd_mem = (isp_cmd_read_mem_t *)params;
      sprintf(msg, "Read memory : ");
      add_log(msg);
      sprintf(buf,"%s %u %u\r\n", ISP_CMD_READ_MEM, rd_mem->addr, rd_mem->length);
      write_response_to_uart(fd, buf, strlen(buf));
      
      int bytes_read = 0;
      while(bytes_read < (3+rd_mem->length)) {
         bytes_read += read_uart_len(fd, buf+bytes_read, 1);
         //printf("Bytes read: %d\n", bytes_read);
      }
      
      if(CMD_OK(buf)) {
         sprintf(msg, "successful\n");
      } else {
         sprintf(msg, "unsuccessful - error code %x\n", buf[0]);
         err = buf[0];
         goto err_done;
      }
      add_log(msg);
      //memcpy(buf, &buf[3], rd_mem->length);

      int error = 0;
      int buf_idx = 3;
      for(int i=0; i<rd_mem->length; i++) {
         if(buf[buf_idx] == rd_mem->src[i]) {
         } else {
            error=1;
            break;
         }
         buf_idx++;
         //fflush(stdout);
      }
      if(error) {
         sprintf(msg, "Data written to RAM is invalid\n");
         err = 1;
      } else {
         sprintf(msg, "Data written to RAM is valid\n");
      }
      add_log(msg);
   } else if(strcmp(cmd, ISP_CMD_PREP_SECT_WRITE) == 0) {
      isp_cmd_prepare_sect_t *prep = (isp_cmd_prepare_sect_t *)params;
      sprintf(msg, "Prepare sectors for write : ");
      add_log(msg);
      sprintf(buf,"%s %d %d\r\n", ISP_CMD_PREP_SECT_WRITE, prep->start_sector, prep->end_sector);
      write_response_to_uart(fd, buf, strlen(buf));

      memset(buf, 0, sizeof(buf));
      int bytes_read = 0;
      while(bytes_read < 3) {
         bytes_read += read_uart_len(fd, buf+bytes_read, 3);
      }
      //read_uart_len(fd, buf, 3);
#ifdef DEBUG
      for(int i=0; i<3; i++) {
         printf("0x%x ",buf[i]);
      }
      printf("\n");
#endif
      if(CMD_OK(buf)) {
         sprintf(msg, "successful\n");
      } else {
         sprintf(msg, "unsuccessful - error code %x\n", buf[0]);
      }
      add_log(msg);
   } else if(strcmp(cmd, ISP_CMD_COPY_RAM_TO_FLASH) == 0) {
      isp_cmd_copy_ram_to_flash_t *copy_ram = (isp_cmd_copy_ram_to_flash_t *)params;

      sprintf(msg, "Copy RAM to flash: flash_addr: 0x%x, RAM addr: 0x%x, length: %d : ", 
               copy_ram->flash_addr, copy_ram->ram_addr, copy_ram->length);
      add_log(msg);

      sprintf(buf,"%s %u %u %u\r\n", ISP_CMD_COPY_RAM_TO_FLASH, copy_ram->flash_addr, 
                                                                copy_ram->ram_addr,
                                                                copy_ram->length);
      write_response_to_uart(fd, buf, strlen(buf));

      memset(buf, 0, sizeof(buf));
      int bytes_read = 0;
      while(bytes_read < 3) {
         bytes_read += read_uart_len(fd, buf+bytes_read, 3);
      }
      //read_uart_len(fd, buf, 3);
#ifdef DEBUG
      for(int i=0; i<3; i++) {
         printf("0x%x ",buf[i]);
      }
      printf("\n");
#endif
      if(CMD_OK(buf)) {
         sprintf(msg, "successful\n");
      } else {
         sprintf(msg, "unsuccessful - error code %x\n", buf[0]);
         err = buf[0];
      }
      add_log(msg);

   } else if(strcmp(cmd, ISP_CMD_GO) == 0) {
      isp_cmd_go_t *pgo = (isp_cmd_go_t *)params;

      sprintf(msg, "Go : ");
      add_log(msg);

      sprintf(buf,"%s %d %s\r\n", ISP_CMD_GO, pgo->addr, pgo->mode);
      //printf("\ncommand = [%s]\n", buf);
      write_response_to_uart(fd, buf, strlen(buf));
      memset(buf, 0, sizeof(buf));
      int bytes_read = 0;
      while(bytes_read < 3) {
         bytes_read += read_uart_len(fd, buf+bytes_read, 3);
      }
      //read_uart_len(fd, buf, 3);
#ifdef DEBUG
      for(int i=0; i<3; i++) {
         printf("0x%x ",buf[i]);
      }
      printf("\n");
#endif
      if(CMD_OK(buf)) {
         sprintf(msg, "successful\n");
      } else {
         sprintf(msg, "unsuccessful - error code %x\n", buf[0]);
         err = buf[0];
      }
      add_log(msg);

   } else if(strcmp(cmd, ISP_CMD_ERASE_SECT) == 0) {
      isp_cmd_erase_sect_t *erase = (isp_cmd_erase_sect_t *)params;

      sprintf(msg, "Erase sector start sector: %d, end sector: %d : ",erase->start_sector, erase->end_sector);
      add_log(msg);

      sprintf(buf,"%s %d %d\r\n", ISP_CMD_ERASE_SECT, erase->start_sector, erase->end_sector);
      write_response_to_uart(fd, buf, strlen(buf));

      memset(buf, 0, sizeof(buf));
      int bytes_read = 0;
      while(bytes_read < 3) {
         bytes_read += read_uart_len(fd, buf+bytes_read, 3);
      }
      //read_uart_len(fd, buf, 3);
#ifdef DEBUG
      for(int i=0; i<3; i++) {
         printf("0x%x ",buf[i]);
      }
      printf("\n");
#endif
      if(CMD_OK(buf)) {
         sprintf(msg, "successful\n");
      } else {
         sprintf(msg, "unsuccessful - error code %x\n", buf[0]);
         err = buf[0];
      }
      add_log(msg);

   } else if(strcmp(cmd, ISP_CMD_BLANK_CHECK) == 0) {
      isp_cmd_blank_check_t *blank = (isp_cmd_blank_check_t *)params;
      sprintf(msg, "Blank check sectors : ");
      add_log(msg);
   } else if(strcmp(cmd, ISP_CMD_READ_PART_ID) == 0) {
      char *part = (char *)params;

      sprintf(msg, "Read Part ID : ");
      add_log(msg);

      sprintf(buf,"%s\r\n", ISP_CMD_READ_PART_ID);
      write_response_to_uart(fd, buf, strlen(buf));
      memset(buf, 0, sizeof(buf));
      int bytes_read = 0;
      while(bytes_read < 9) {
         bytes_read += read_uart_len(fd, buf+bytes_read, 9);
      }
      //read_uart_len(fd, buf, 9);
#ifdef DEBUG
      for(int i=0; i<9; i++) {
         printf("0x%x ",buf[i]);
      }
      printf("\n");
#endif
      if(CMD_OK(buf)) {
         sprintf(msg, "successful\n");
      } else {
         sprintf(msg, "unsuccessful - error 0x%x\n", buf[0]);
         err = buf[0];
         goto err_done;
      }
      add_log(msg);

      for(int i=3; i<7; i++) {
         part[i-3] = buf[i];
      }
      part[4] ='\0';

   } else if(strcmp(cmd, ISP_CMD_READ_BOOT_VERSION) == 0) {
      uint32_t *boot_version = (uint32_t *)params;

      sprintf(msg, "Read boot code version: ");
      add_log(msg);

      sprintf(buf,"%s\r\n", ISP_CMD_READ_BOOT_VERSION);
      write_response_to_uart(fd, buf, strlen(buf));
      
      int bytes_read = 0;
      while(bytes_read < 10) {
         bytes_read += read_uart_len(fd, buf+bytes_read, 1);
      }
     
#ifdef DEBUG
      for(int i=0; i<10; i++) {
         printf("0x%x ", buf[i]);
      }
      printf("\n");
#endif      
      if(CMD_OK(buf)) {
         sprintf(msg, "successful\n");
      } else {
         sprintf(msg, "unsuccessful - error 0x%x\n", buf[0]);
         err = buf[0];
         goto err_done;
      }
      add_log(msg);

      char value[3];
      value[0] = buf[3];
      value[1] = '\0';
      uint8_t byte1 = atoi(value);
      value[0] = buf[6];
      value[1] = buf[7];
      value[2] = '\0';
      uint8_t byte2 = atoi(value);
      //printf("byte1 = 0x%x, byte2 = 0x%x\n", byte1, byte2);
      *boot_version = ((byte1)<<8)|(byte2);
      
   } else if(strcmp(cmd, ISP_CMD_COMPARE_ADDR) == 0) {
      isp_cmd_compare_t *cmp = (isp_cmd_compare_t *)params;
      printf("Compare memory address : ");
   } else if(strcmp(cmd, ISP_CMD_READ_UID) == 0) {
      int idx;
      int uuid_idx = 0;
      char *uuid = (char *)params;

      sprintf(msg, "Read UID :");
      add_log(msg);

      sprintf(buf,"%s\r\n", ISP_CMD_READ_UID);
      write_response_to_uart(fd, buf, strlen(buf));
      memset(buf, 0, sizeof(buf));
      int bytes_read = 0;
      while(bytes_read < 50) {
         bytes_read += read_uart_len(fd, buf+bytes_read, 1);
      }
      //read_uart_len(fd, buf, 50);
#ifdef DEBUG
      for(int i=0; i<50; i++) {
         printf("0x%x ", buf[i]);
      }
      printf("\n");
#endif      
      if(CMD_OK(buf)) {
         sprintf(msg, "successful\n");
      } else {
         sprintf(msg, "unsuccessful - error 0x%x\n", buf[0]);
         err = buf[0];
         goto err_done;
      }
      add_log(msg);

      uuid_idx = 0;
      idx = 3;
      for(uuid_idx=0; uuid_idx<50; ) {
         uuid[uuid_idx++] = buf[idx++];
         if(buf[idx] == 0xd || buf[idx+1] == 0xa) {
            if(idx >= 48) {
               uuid[uuid_idx]='\0';
               break;
            }
            uuid[uuid_idx++] = '-';
            idx+=2;
         }
      }

   } else if(strcmp(cmd, ISP_CMD_READ_CRC) == 0) {
      isp_cmd_read_chksum_t *chk = (isp_cmd_read_chksum_t *)params;

      sprintf(msg, "Read CRC : ");
      add_log(msg);

      isp_cmd_copy_ram_to_flash_t *copy_ram = (isp_cmd_copy_ram_to_flash_t *)params;
      sprintf(buf,"%s %d %d\r\n", ISP_CMD_READ_CRC, chk->addr, chk->length);
      write_response_to_uart(fd, buf, strlen(buf));

      memset(buf, 0, sizeof(buf));
      int bytes_read = 0;
      while(bytes_read < 15) {
         bytes_read += read_uart_len(fd, buf+bytes_read, 1);
      }
      
      //read_uart_len(fd, buf, 15);
#ifdef DEBUG
      for(int i=0; i<15; i++) {
         printf("0x%x ",buf[i]);
      }
      printf("\n");
#endif
      if(CMD_OK(buf)) {
         sprintf(msg, "successful\n");
      } else {
         sprintf(msg, "unsuccessful - error code %x\n", buf[0]);
         err = buf[0];
         goto err_done;
      }
      add_log(msg);
      int idx=0;
      char number[20]="";
      for(idx=3; idx<13; idx++) {
         number[idx-3] = buf[idx];
      }
      number[idx-3]='\0';
      sprintf(msg, "=== CRC32 string %s\n", number);
      add_log(msg);
      uint32_t crc32 = atoi(number);
      sprintf(msg, "=== CRC32 value %0x\n", crc32);
      add_log(msg);
   }
   
err_done:
   return err;
}

static int initiate_isp(int fd)
{
   unsigned char buf[1024];
   int err = 0;
   int bytes_read = 0;

   /* Send the ISP cmd */
   buf[0] = 0x2;
   buf[1] = 3;
   buf[2] = 0xff; // ISP command
   buf[3] = 1;
   buf[4] = 0x3;
   
   err = write_response_to_uart(fd, buf, 5);
   if(err) {
      sprintf(buf, "write_response_to_uart error: UART ISP INITIATE\n");
      add_log(buf);
      err = -1;
      goto initiate_isp_error;
   }

   sprintf(buf, "Entering ISP mode ...\n");
   add_log(buf);

initiate_isp_error:

   return err;
}

size_t get_file_size(const char* filename)
{
   struct stat st;
   if(stat(filename, &st) != 0) {
      perror("Error ");
      exit(1);
   }
   return st.st_size;   
}

int main(int argc, char **argv)
{
   char msg[1024];
   char *portname = "/dev/ttyS1";
   char *image_name = NULL;
   char *reset_name = NULL;
   int fd = -1;
   int c;
   int err = 0;
   int input = -1;
   uint32_t part_id;
   uint32_t echo = 0;
   uint32_t boot_version;
   uint8_t *image_buffer = NULL;
   uint8_t *tmp;
   uint8_t buf[1024];

   size_t len_image = 0;
   size_t calc_len = 0;
   isp_cmd_unlock_t unlock;
   isp_cmd_prepare_sect_t prep;
   isp_cmd_erase_sect_t erase;
   isp_cmd_write_ram_t wr_ram;
   isp_cmd_read_chksum_t rd_crc;
   isp_cmd_copy_ram_to_flash_t cp_flash;
   isp_cmd_go_t go_cmd;

   printf("Stopping sensesdk-mgr ...\n");
   system("systemctl stop sensesdk-mgr");

   while((c = getopt (argc, argv, "i:r:")) != -1) {
      
      switch (c) {
         case 'i':
            image_name = optarg;
            break;
         case 'r':
            reset_name = optarg;
            break;
         case '?':
            if(optopt == 'i') {
               printf("Option -%c requires an argument.\n", optopt);
            } else if (isprint (optopt)) {
               printf("Unknown option `-%c'.\n", optopt);
            } else {
               printf("Unknown option character `\\x%x'.\n",optopt);
            }
            exit(1);

         default:
            exit(1);
         }
   }

   if(!image_name) {
      sprintf(msg,"Image needed to proceed\n");
      add_log(msg);
      exit(10);
   }

   len_image = get_file_size(image_name);
   if(len_image == 0) {
      sprintf(msg,"Image file length is 0\n");
      add_log(msg);
      exit(20);
   }

   if(!reset_name) {
      sprintf(msg,"RESET Image needed to proceed\n");
      add_log(msg);
      exit(30);
   }

   /* Make length of image multiples of a word (4-bytes) aligned */
   if((len_image%SECTOR_SIZE) != 0) {
      calc_len = len_image + (4096-len_image%SECTOR_SIZE);
   } else {
      calc_len = len_image;
   }
   sprintf(msg, "Filename name: %s\n", image_name);
   add_log(msg);
   sprintf(msg, "File length: %ld\n", calc_len);
   add_log(msg);

   input = open(image_name, O_RDONLY);
   
   image_buffer = (uint8_t *)malloc(calc_len);
   memset(image_buffer, 0, calc_len);

   size_t read_len = read(input, image_buffer, len_image);
   if(read_len != len_image) {
      sprintf(msg, "Read mismatch in length read (%ld), expected(%ld)\n", read_len, len_image);
      add_log(msg);
      err = 100;
      goto uart_error;
   }

   fd = open(portname, O_RDWR | O_NOCTTY | O_SYNC);
   if (fd < 0) {
      sprintf(msg, "Error opening %s: %s\n", portname, strerror(errno));
      add_log(msg);
      err = 200;
      goto uart_error;
   }
   /*baudrate 115200, 8 bits, no parity, 1 stop bit */
   set_interface_attribs(fd, B115200);
   //set_mincount(fd, 0);                /* set to pure timed read */

   tcflush(fd, TCIFLUSH);

   initiate_isp(fd);

   usleep(2000000);
   sprintf(msg, "Initiate communication with ISP ...\n");
   add_log(msg);
   err = write_response_to_uart(fd, ISP_START, strlen(ISP_START));
   if(err) {
      sprintf(msg, "write_response_to_uart error: ISP_START\n");
      add_log(msg);
      err = 300;
      goto uart_error;
   }

   sprintf(msg, "Checking for Synchronization message from ISP ...\n");
   add_log(msg);
   err = read_response_from_uart(fd, ISP_SYNCRONIZED_RESPONSE);
   if(err) {
      sprintf(msg, "Synchronized response not OK\n");
      add_log(msg);
      err = 400;
      goto uart_error;
   }
   sprintf(msg, "We got the 'Synchronized' from ISP\n");
   add_log(msg);

   sprintf(msg, "Writing 'Synchronized' response to ISP\n");
   add_log(msg);
   err = write_response_to_uart(fd, ISP_SYNCRONIZED_RESPONSE, strlen(ISP_SYNCRONIZED_RESPONSE));
   if(err) {
      sprintf(msg, "write_response_to_uart error: SYNCRONIZED_RESPONSE\n");
      add_log(msg);
      err = 500;
      goto uart_error;
   }
   
   sprintf(msg, "Checking Synchronized status ...\n");
   add_log(msg);
   sprintf(buf,"%s%s", ISP_SYNCRONIZED_RESPONSE, ISP_OK_RESPONSE);
   err = read_response_from_uart(fd, buf);
   if(err) {
      sprintf(msg, "Synchronized response status not OK\n");
      add_log(msg);
      err = 600;
      goto uart_error;
   }
   sprintf(msg, "We got the 'Synchronized OK'\n");
   add_log(msg);

   sprintf(msg, "Setting the clock speed of crystal to 50MHz ...\n");
   add_log(msg);
   err = write_response_to_uart(fd, ISP_FIFTY_MHZ, strlen(ISP_FIFTY_MHZ));
   if(err) {
      sprintf(msg, "write_response_to_uart error: 50MZ  \n");
      add_log(msg);
      err = 700;
      goto uart_error;
   }
   
   sprintf(msg, "Checking setting 50MHz status ...\n");
   add_log(msg);
   sprintf(buf,"%s%s", ISP_FIFTY_MHZ, ISP_OK_RESPONSE);
   err = read_response_from_uart(fd, buf);
   if(err) {
      sprintf(msg, "Setting 50MHz status not OK\n");
      add_log(msg);
      err = 800;
      goto uart_error;
   }
   sprintf(msg, "We got the '50000 OK'\n");
   add_log(msg);

   sprintf(msg, "Autobauding is good ...\n");
   add_log(msg);

   send_cmd_to_isp(fd,  ISP_CMD_ECHO, &echo);

   send_cmd_to_isp(fd, ISP_CMD_READ_BOOT_VERSION, &boot_version);
   sprintf(msg, "Boot Version: Major: %d, minor: %d\n", (boot_version>>8),boot_version&0xff);
   add_log(msg);

   send_cmd_to_isp(fd, ISP_CMD_READ_PART_ID, buf);
   sprintf(msg, "PART ID: %s\n", buf);
   add_log(msg);

   send_cmd_to_isp(fd, ISP_CMD_READ_UID, buf);
   sprintf(msg, "UID: %s\n", buf);
   add_log(msg);

   memset(&unlock, 0, sizeof(unlock));
   unlock.code = 23130;
   send_cmd_to_isp(fd, ISP_CMD_UNLOCK_FLASH, &unlock);

   size_t totlen = calc_len;
   size_t wr_size = SECTOR_SIZE;
   tmp = image_buffer;

   memset(&wr_ram, 0, sizeof(wr_ram));
   memset(&cp_flash, 0, sizeof(cp_flash));
   
   int idx = 0;
   uint32_t sect_no = 0;
   while( totlen > 0) {

      memset(&prep, 0, sizeof(prep));
      prep.start_sector = sect_no;
      prep.end_sector =  sect_no;
      err = send_cmd_to_isp(fd, ISP_CMD_PREP_SECT_WRITE, &prep);
      if(err) {
         sprintf(msg, "iteration %d: Prepare write/erase error: %d\n", idx, err);
         add_log(msg);
         err = 900;
         goto uart_error;
      }
      memset(&erase, 0, sizeof(erase));
      erase.start_sector = sect_no;
      erase.end_sector = sect_no;
      err = send_cmd_to_isp(fd, ISP_CMD_ERASE_SECT, &erase);
      if(err) {
         sprintf(msg, "iteration %d: Erase error: %d\n", idx, err);
         add_log(msg);
         err = 1000;
         goto uart_error;
      }
      wr_ram.start_addr = 0x2000000;
      wr_ram.length = SECTOR_SIZE;
      wr_ram.buffer = tmp;
      err = send_cmd_to_isp(fd, ISP_CMD_WRITE_RAM, &wr_ram);
      if(err) {
         sprintf(msg, "iteration %d: Write-to-RAM error: %d\n", idx, err);
         add_log(msg);
         err = 1100;
         goto uart_error;
      }

      memset(&prep, 0, sizeof(prep));
      prep.start_sector = sect_no;
      prep.end_sector =  sect_no;
      err = send_cmd_to_isp(fd, ISP_CMD_PREP_SECT_WRITE, &prep);
      if(err) {
         sprintf(msg, "iteration %d: Prepare write/erase error: %d\n", idx, err);
         add_log(msg);
         err = 1200;
         goto uart_error;
      }

      cp_flash.ram_addr = 0x2000000;
      cp_flash.flash_addr = sect_no*SECTOR_SIZE;
      cp_flash.length = SECTOR_SIZE;
      err = send_cmd_to_isp(fd, ISP_CMD_COPY_RAM_TO_FLASH, &cp_flash);
      if(err) {
         sprintf(msg, "iteration %d: Copy-to-RAM error: %d\n", idx, err);
         add_log(msg);
         err = 1300;
         goto uart_error;
      }
      idx++;
      sect_no++;
      tmp += SECTOR_SIZE;
      totlen -= SECTOR_SIZE;
      sprintf(msg, "=======> Programmed sector: %d\n", sect_no);
      add_log(msg);
   }

   read_uart_len(fd, buf, 20);

   close(input);
   free(image_buffer);
   input = open(reset_name, O_RDONLY);
   len_image = get_file_size(reset_name);
   image_buffer = malloc(len_image);
   read_len = read(input, image_buffer, len_image);
   if(read_len != len_image) {
      sprintf(msg, "Read length error\n");
      add_log(msg);
      err = 1400;
      goto uart_error;
   }

   /* As a workaround for the ISP reset not working, we are loading
    * and WDT to trigger a RESET with the timeout of 3 seconds.
    */
   wr_ram.start_addr = 0x2000000;
   wr_ram.length = len_image;
   wr_ram.buffer = image_buffer;
   err = send_cmd_to_isp(fd, ISP_CMD_WRITE_RAM, &wr_ram);
   if(err) {
      sprintf(msg, "iteration %d: Write-to-RAM error: %d\n", idx, err);
      add_log(msg);
      err = 1500;
      goto uart_error;
   }

   /* Jump to the WDT application code and executte */
   memset(&go_cmd, 0, sizeof(go_cmd));
   go_cmd.addr = 0x2000450;
   go_cmd.mode = ARM_MODE;
   err = send_cmd_to_isp(fd, ISP_CMD_GO, &go_cmd);
   if(err) {
      sprintf(msg, "GO error: %d\n", err);
      add_log(msg);
      err = 1600;
      goto uart_error;
   }  

   int alive = 0;
   int error_count = 0;
   sprintf(msg, "Checking for Power board to come up again ...\n");
   add_log(msg);
   while(1) {
      int bytes_read;
      uint8_t start_byte;
      uint8_t length_pkt;
      //static int read_uart_len(int fd, char *response, int len)
      bytes_read = read_uart_len(fd, &start_byte, 1);
      if(bytes_read == 1 && start_byte == 0x2) {
         //printf("Found START byte\n");
         while(1) {
            bytes_read = read_uart_len(fd, &length_pkt, 1);
            if(bytes_read == 1) break;
         }
         if(length_pkt > 12) {
            usleep(1000);
            continue;
         }
         int num_read=0;
         while(1) {
            bytes_read = read_uart_len(fd, &buf[num_read], 1);
            if(bytes_read == 0) {
               error_count++;
               if(error_count == 1000) {
                  err = 1700;
                  goto uart_error;
               }
            }
            num_read += bytes_read;
            if(num_read == length_pkt) break;
         }
#ifdef DEBUG
         printf("[");
         for(int i=0; i<length_pkt; i++)
            printf(" 0x%x ", buf[i]);
         printf("]\n");
#endif
         if((buf[0]>>4) == 0x1 && buf[length_pkt-1] == 0x3) {
            sprintf(msg, "Firmware upgrade file [%s] successful!\n", image_name);
            add_log(msg);
            alive = 1;
            break;
         }
      } else if(bytes_read == 0) {
         error_count++;
         if(error_count == 1000) {
            sprintf(msg, "Power board is not responding after firmware upgrade\n");
            add_log(msg);
            err = 1800;
            goto uart_error;
         }
      }

      if(alive) {
         break;
      }
   }


uart_error:
   if(input > 0) {
      close(input);
   }
   if(fd > 0) {
      close(fd);
   }
   
   if(image_buffer) {
      free(image_buffer);
   }
   return err;
}
