#include <errno.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdint.h>
#include <ctype.h>
#include <sys/stat.h>

static int set_interface_attribs(int fd, int speed)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    tty.c_cflag |= (CLOCAL | CREAD);    /* ignore modem controls */
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

    /* setup for non-canonical mode */
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tty.c_oflag &= ~OPOST;

    /* fetch bytes as they become available */
    tty.c_cc[VMIN] = 0;
    tty.c_cc[VTIME] = 5;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        printf("Error from tcsetattr: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

static void set_mincount(int fd, int mcount)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error tcgetattr: %s\n", strerror(errno));
        return;
    }

    tty.c_cc[VMIN] = mcount ? 1 : 0;
    tty.c_cc[VTIME] = 5;        /* half second timer */

    if (tcsetattr(fd, TCSANOW, &tty) < 0)
        printf("Error tcsetattr: %s\n", strerror(errno));
}

static int write_response_to_uart(int fd, const char *response, size_t len)
{
   int wlen;
   int count = 0;
   
   /* simple output */
   
   while(count < len) {
      wlen = write(fd, response+count, 1);
      if (wlen != 1) {
         printf("Error from write: %d, %d\n", wlen, errno);
         return -1;
      }
      count++;
   }
   tcdrain(fd);    /* delay for output */
   return 0;
}

static void get_version(int fd)
{
   int err = 0;
   uint8_t buf[10];

   /* Send the UPGRADE EXT LEDS cmd */
   buf[0] = 0x2;
   buf[1] = 3;
   buf[2] = 0x5; /* CMD_VERSION */
   buf[3] = 1;
   buf[4] = 0x3;
   
   err = write_response_to_uart(fd, buf, 5);
   if(err) {
      printf("write_response_to_uart error: CMD_VERSION\n");
      err = -1;
   }
   
}

static int read_uart_len(int fd, char *response, int len)
{
   unsigned char *tmp;
   int totlen;
   int rdlen;

   memset(response, 0, sizeof(*response));
   tmp = response;
   totlen = 0;
   while(totlen < len) {
      rdlen = read(fd, tmp, 1);
      if (rdlen > 0) {
         totlen += rdlen;
         tmp += rdlen;
         response[totlen]='\0';
         //printf("Read total %d: \n", totlen);
         usleep(1000);

      } else if (rdlen < 0) {
         //printf("Error from read: %d: %s\n", rdlen, strerror(errno));
      } else {  /* rdlen == 0 */
         //printf("Timeout from read\n");
         return 0;
      }
      
   }
   return totlen;
}

int main(int argc, char **argv)
{
   char *portname = "/dev/ttyS1";
   int c;
   uint8_t start_byte;
   uint8_t len_pkt;
   uint8_t major = 0;
   uint8_t minor = 0;
   uint8_t sub_minor = 0;
   int timeout_count = 50;

   printf("Stopping sensesdk-mgr ...\n");
   system("systemctl stop sensesdk-mgr");

   int fd = open(portname, O_RDWR | O_NOCTTY | O_SYNC);
   if (fd < 0) {
      printf("Error opening %s: %s\n", portname, strerror(errno));
      return -1;
   }
   /*baudrate 115200, 8 bits, no parity, 1 stop bit */
   set_interface_attribs(fd, B115200);
   //set_mincount(fd, 0);                /* set to pure timed read */

   tcflush(fd, TCIFLUSH);


   get_version(fd);

   int len = 0;
   while(1) {
read_again:
      len = read_uart_len(fd, &start_byte, 1);
      if(len > 0) {
         if(start_byte == 0x2) {
            while(1) {
               //printf("get lenth pkt: timeout_count %d\n",timeout_count);
               len = read_uart_len(fd, &len_pkt, 1);
               if(len_pkt == 4) {
                  //printf("get lenth pkt=4\n");
                  len = 0;
                  while(len <= 0) {
                     //printf("get major \n");
                     len = read_uart_len(fd, &major, 1);
                     if(len == 0) {
                        timeout_count--;
                        if(timeout_count == 0) {
                           goto got_version;
                        }
                     }
                  }
                  len = 0;
                  while(len <= 0) {
                     //printf("get minor \n");
                     len = read_uart_len(fd, &minor, 1);
                     if(len == 0) {
                        timeout_count--;
                        if(timeout_count == 0) {
                           goto got_version;
                        }
                     }
                  }
                  len = 0;
                  while(len <= 0) {
                     //printf("get sub_minor \n");
                     len = read_uart_len(fd, &sub_minor, 1);
                     if(len == 0) {
                        timeout_count--;
                        if(timeout_count == 0) {
                           goto got_version;
                        }
                     }
                  }
                  goto got_version;
               } else {
                  timeout_count--;
                  if(timeout_count == 0) {
                     goto got_version; /* Nothing */
                  }
                  goto read_again;
               }
            }
          }
       } else {
          usleep(2000);
       }
   }
got_version:

   printf("%d.%d.%d\n", major, minor, sub_minor);
   close(fd);
   return 0;
}
