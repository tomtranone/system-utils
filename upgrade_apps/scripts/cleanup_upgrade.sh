#!/bin/bash

LIST=`ls -w 0 -a /data/.upgrade`

for i in $LIST
do
	if [ "$i" != "." ] && [ "$i" != ".." ] && [ "$i" != "sbc_upgrade" ] && [ "$i" != "active_bank" ] && [ "$i" != "cleanup_upgrade.sh" ]; then
		printf "Cleaning upgrade file %s\n" $i
		rm -rf /data/.upgrade/$i
	fi
done
