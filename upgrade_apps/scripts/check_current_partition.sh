#!/bin/bash

ROOTFS0=`sfdisk --part-uuid /dev/sda 2 |  awk '{print tolower($0)}'`
ROOTFS1=`sfdisk --part-uuid /dev/sda 3 |  awk '{print tolower($0)}'`

echo "ROOTFS0 UUID: "$ROOTFS0 
echo "ROOTFS1 UUID: "$ROOTFS1

if [ "$ROOTFS0" = "" ]; then
   printf "ERROR:ERROR: Big error - this should never be empty !!!!!"
fi

if [ "$ROOTFS1" = "" ]; then
   printf "Please use new FLASH layout with create_flash_partition.sh\n"
   exit 1
fi

if [ ! -f /data/.upgrade/active_bank ]; then
   CURR_ROOTFS=`cat /boot/loader/entries/boot0.conf | grep "PARTUUID=" | awk '{print $2}' | sed -e 's/root=PARTUUID=//g'`
   if [ "$ROOTFS0" = "$CURR_ROOTFS" ]; then
      printf "Current active rootfs0 being used\n"
      ACTIVE="0"
   elif [ "$ROOTFS1" = "$CURR_ROOTFS" ]; then
      printf "Current active rootfs1 being used\n"
      ACTIVE="1"
   else
      printf "Current active doesn't match anyting - BAD BAD\n"
      exit 1
   fi

   echo "$ACTIVE" > /data/.upgrade/active_bank
fi


printf "Done checking !\n"


