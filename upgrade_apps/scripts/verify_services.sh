printf "PTP4L service       : "
ssh root@192.168.1.20 systemctl is-active ptp4l
printf "PHC2SYS service     : "
ssh root@192.168.1.20 systemctl is-active phc2sys
printf "SBC_UPGRADE service : "
ssh root@192.168.1.20 systemctl is-active sbc_upgrade
printf "SDK service         : "
ssh root@192.168.1.20 systemctl is-active sensesdk-mgr

printf "File: sensesdk-mgr.service\n"
ssh root@192.168.1.20 cat /lib/systemd/system/sensesdk-mgr.service
printf "File: sense1_mgr.service\n"
ssh root@192.168.1.20 cat /lib/systemd/system/sense1_mgr.service

printf "File listing: /data/.upgrade\n"
printf "============================\n"
ssh root@192.168.1.20 ls -al /data/.upgrade
printf "============================\n"

printf "File listing: /data/tools\n"
printf "============================\n"
ssh root@192.168.1.20 ls -al /data/tools
printf "============================\n"
