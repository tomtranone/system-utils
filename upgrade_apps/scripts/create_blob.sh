#!/bin/bash

# On the command line provide the name, i.e., LINUX=linux_kernel
# It can be the absolute path. (Can be a symbolic link to images from Yocto build)

function usage()
{
   printf "usage: ./create_blob.sh ROOTFS=<rootfs_file.EXT4> LINUX=<Linux_Kernel, i.e., bzImage>\n\n"
   exit 1
}

if [[ "$1" == "LINUX"* ]]; then
   LINUX_IMAGE=`echo $1 | sed 's/LINUX=//g'`
fi

if [[ "$2" == "LINUX"* ]]; then
   LINUX_IMAGE=`echo $2 | sed 's/LINUX=//g'`
fi

if [ "$LINUX_IMAGE" = "" ]; then
   usage
fi


if [[ "$1" == "ROOTFS"* ]]; then
   ROOTFS_IMAGE=`echo $1 | sed 's/ROOTFS=//g'`
fi

if [[ "$2" == "ROOTFS"* ]]; then
   ROOTFS_IMAGE=`echo $2 | sed 's/ROOTFS=//g'`
fi

if [ "$ROOTFS_IMAGE" = "" ]; then
   usage
fi

# This should not be replaced.
CHECK_SCRIPT=check_current_partition.sh
UPDATE_SCRIPT=sw_upgrade.sh
GEN_IMAGE=gen_img
UPGRADE_LIB=libupgrade.so
BLOB_PARSE=blob_parse
OUTPUT_BIN=blob.bin
RSYNC=rsync
CGOSDRV=cgosdrv.ko
LIBCGOS=libcgos.so
CGUTLCMD=cgutlcmd
ISP=uart_isp
FW_BIN=power_board.bin
RESET_BIN=reset.bin
LED_BIN=led_blink
SERIAL_MENU=serial_menu
GET_VERSION=get_version
GET_BOARD_REV=get_board_rev
PWR_BRD_INFO=pwr_brd_info
RESTORE=restore.sh
TIMELIMIT=timelimit
CLEANUP_SCRIPT=cleanup_upgrade.sh

CLEANUP_SVC=cleanup_upgrade.service
SDK_SVC=sensesdk-mgr.service
DEMO_SDK_SVC=sense1_mgr.service
UPG_SVC=sbc_upgrade.service

SHORT_LK_NAME=lk.bin
SHORT_RFS_NAME=rootfs

UPGRADE_NAME=upgrade

printf "Creating an upgrade blob image ...\n"
printf "Linux kernel: %s\n" $LINUX_IMAGE
printf "RootFS kernel: %s\n" $ROOTFS_IMAGE
printf "Update script: %s\n" $UPDATE_SCRIPT

if [ ! -f $LINUX_IMAGE ]; then
   printf "Linux image : [$LINUX_IMAGE] doesn't exist\n"
   printf "Please point(symbolic link) to a valid %s\n" $LINUX_IMAGE
fi

if [ ! -f $ROOTFS_IMAGE ]; then
   printf "RootFS image : [$ROOTFS_IMAGE] doesn't exist\n"
   printf "Please point(symbolic link) rootfs image %s\n" $ROOTFS_IMAGE
   exit 1
fi

if [ ! -f $UPDATE_SCRIPT ]; then
   printf "Update script : [$UPDATE_SCRIPT] doesn't exist\n"
   printf "Please find the an current %s script\n" $UPDATE_SCRIPT
   exit 1
fi

if [ ! -f $GEN_IMAGE ]; then
   printf "gen_img doesn't exist\n"
   printf "Please point(symbolic link)to %s\n" $GEN_IMAGE
   exit 1
fi

if [ ! -f $UPGRADE_LIB ]; then
   printf "libupgrade.so doesn't exist\n"
   printf "Please point(symbolic link)to %s\n" $UPGRADE_LIB
   exit 1
fi

if [ ! -f $BLOB_PARSE ]; then
   printf "blob_parse doesn't exist\n"
   printf "Please point(symbolic link)to %s\n" $BLOB_PARSE
   exit 1
fi

if [ ! -f $CGOSDRV ]; then
   printf "%s doesn't exist\n" $CGOSDRV
   printf "Please point(symbolic link)to %s\n" $CGOSDRV
   exit 1
fi

if [ ! -f $LIBCGOS ]; then
   printf "%s doesn't exist\n" $LIBCGOS
   printf "Please point(symbolic link)to %s\n" $LIBCGOS
   exit 1
fi

if [ ! -f $CGUTLCMD ]; then
   printf "%s doesn't exist\n" $CGUTLCMD
   printf "Please point(symbolic link)to %s\n" $CGUTLCMD
   exit 1
fi

if [ ! -f $ISP ]; then
   printf "%s doesn't exist\n" $ISP
   printf "Please point(symbolic link)to %s\n" $ISP
   exit 1
fi

if [ ! -f $FW_BIN ]; then
   printf "%s doesn't exist\n" $FW_BIN
   printf "Please point(symbolic link)to %s\n" $FW_BIN
   exit 1
fi

if [ ! -f $RESET_BIN ]; then
   printf "%s doesn't exist\n" $RESET_BIN
   printf "Please point(symbolic link)to %s\n" $RESET_BIN
   exit 1
fi

if [ ! -f $LED_BIN ]; then
   printf "%s doesn't exist\n" $LED_BIN
   printf "Please point(symbolic link)to %s\n" $LED_BIN
   exit 1
fi

if [ ! -f $SERIAL_MENU ]; then
   printf "%s doesn't exist\n" $SERIAL_MENU
   printf "Please point(symbolic link)to %s\n" $SERIAL_MENU
   exit 1
fi

if [ ! -f $GET_VERSION ]; then
   printf "%s doesn't exist\n" $GET_VERSION
   printf "Please point(symbolic link)to %s\n" $GET_VERSION
   exit 1
fi

if [ ! -f $GET_BOARD_REV ]; then
   printf "%s doesn't exist\n" $GET_BOARD_REV
   printf "Please point(symbolic link)to %s\n" $GET_BOARD_REV
   exit 1
fi

if [ ! -f $PWR_BRD_INFO ]; then
   printf "%s doesn't exist\n" $PWR_BRD_INFO
   printf "Please point(symbolic link)to %s\n" $PWR_BRD_INFO
   exit 1
fi

if [ ! -f $RESTORE ]; then
   printf "%s doesn't exist\n" $RESTORE
   printf "Please point(symbolic link)to %s\n" $RESTORE
   exit 1
fi

if [ ! -f $TIMELIMIT ]; then
   printf "%s doesn't exist\n" $TIMELIMIT
   printf "Please point(symbolic link)to %s\n" $TIMELIMIT
   exit 1
fi

if [ ! -f $CLEANUP_SCRIPT ]; then
   printf "%s doesn't exist\n" $CLEANUP_SCRIPT
   printf "Please point(symbolic link)to %s\n" $CLEANUP_SCRIPT
   exit 1
fi

if [ ! -f $CLEANUP_SVC ]; then
   printf "%s doesn't exist\n" $CLEANUP_SVC
   printf "Please point(symbolic link)to %s\n" $CLEANUP_SVC
   exit 1
fi

if [ ! -f $SDK_SVC ]; then
   printf "%s doesn't exist\n" $SDK_SVC
   printf "Please point(symbolic link)to %s\n" $SDK_SVC
   exit 1
fi

if [ ! -f $DEMO_SDK_SVC ]; then
   printf "%s doesn't exist\n" $DEMO_SDK_SVC
   printf "Please point(symbolic link)to %s\n" $DEMO_SDK_SVC
   exit 1
fi

if [ ! -f $UPG_SVC ]; then
   printf "%s doesn't exist\n" $UPG_SVC
   printf "Please point(symbolic link)to %s\n" $UPG_SVC
   exit 1
fi

printf "Cleaning previous binaries ...\n"
rm -fr *.bz2 rootfs || true

# Copy the files here locally in this directory.
printf "Copying $LINUX_IMAGE locally ...\n"
rsync -aL --info=progress2 $LINUX_IMAGE $SHORT_LK_NAME
if [ $? != 0 ]; then
   printf "rsync error: Copy to local directory [%s]\n", $SHORT_LK_NAME
   exit 1
fi

printf "Copying $ROOTFS_IMAGE locally ...\n"
rsync -aL --info=progress2 $ROOTFS_IMAGE $SHORT_RFS_NAME
if [ $? != 0 ]; then
   printf "rsync error: Copy to local directory [%s]\n", $SHORT_RFS_NAME
   exit 1
fi

printf "Compressing rootfs image ...\n"
SIZE=`du -sk $SHORT_RFS_NAME | cut -f 1`
tar -chvf - $SHORT_RFS_NAME -P | pv -p -s ${SIZE}k | bzip2 -c > $SHORT_RFS_NAME.tar.bz2

printf "Generating the upgrade blob ...\n"
LD_LIBRARY_PATH=./ ./gen_img -l $SHORT_LK_NAME -r $SHORT_RFS_NAME.tar.bz2 -o $OUTPUT_BIN

tar -chvjf $UPGRADE_NAME.tar.bz2 $OUTPUT_BIN $UPGRADE_LIB $BLOB_PARSE $UPDATE_SCRIPT $CHECK_SCRIPT $RSYNC $CGOSDRV $LIBCGOS $CGUTLCMD $ISP $FW_BIN $RESET_BIN $LED_BIN $SERIAL_MENU $GET_VERSION $GET_BOARD_REV $PWR_BRD_INFO $RESTORE $TIMELIMIT $CLEANUP_SCRIPT $CLEANUP_SVC $SDK_SVC $DEMO_SDK_SVC $UPG_SVC

rm -fr $SHORT_LK_NAME $SHORT_RFS_NAME $SHORT_RFS_NAME.tar.bz2 $OUTPUT_BIN

printf "Complete generating upgrade image.\n" $UPGRADE_NAME.tar.bz2






