#!/bin/bash

ACTIVE_BANK=`cat /data/.upgrade/active_bank`


usage()
{
   printf "./restore.sh <bank_number>\n"
   exit 1
}

printf "Active bank : %d\n" $ACTIVE_BANK

if [ "$ACTIVE_BANK" = "0" ]; then
   echo -e "default boot1\ntimeout 5 " > /boot/loader/loader.conf
   echo 1 > /data/.upgrade/active_bank
   
else
   echo -e "default boot0\ntimeout 5 " > /boot/loader/loader.conf
   echo 0 > /data/.upgrade/active_bank
fi

ACTIVE_BANK=`cat /data/.upgrade/active_bank`
printf "Reverting to previous boot partition: %d\n" $ACTIVE_BANK
printf "Please reboot for changes to take effect\n"
