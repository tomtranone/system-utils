# Package as part of the upgrade

green_led=2
red_led=3

function blink_led()
{
   count=0
   led_no=$1
   state=$2
   iteration=$3

   while [ $count -lt $iteration ]; 
   do
      nice -n -20 $UPG_DIR/led_blink -l $1 -s $state
      if [ $state -eq 1 ]; then
         state=0
      else
         state=1
      fi
      sleep .2s
      count=$(($count+1))
   done
}


#!/bin/bash
UPG_DIR=/data/.upgrade
ACTIVE_BANK=`cat $UPG_DIR/active_bank`

export LD_LIBRARY_PATH=$UPG_DIR

logger "[sbc_upgrade]: Executing sw_upgrade.sh ..."
logger "[sbc_upgrade]: !!!!!!!!!!!!!! PLEASE DO NOT POWER OFF UNIT !!!!!!!!!!!!!!!!!!"

blink_led $green_led 0 10
$UPG_DIR/check_current_partition.sh
if [ $? != 0 ]; then
   logger "[sbc_upgrade]: Cannot proceed, wrong FLASH layout"
   exit 1
fi

SENSE_ACTIVE=`systemctl is-active sensesdk-mgr`
if [ "$SENSE_ACTIVE" == "active" ]; then
   logger "[sbc_upgrade]: Stopping sensesdk-mgr service ..."
   systemctl stop sensesdk-mgr.service
fi

# Remove 'demo' autostart
SENSE_ACTIVE=`systemctl is-active sensesdk-mgr`
if [ "$SENSE_ACTIVE" == "active" ]; then
   logger "[sbc_upgrade]: Stopping sensesdk-mgr service ..."
   systemctl stop sensesdk-mgr.service
fi

function clean_files()
{
   rm -rf *~ *.bz2 *.bin *.sh rootfs $UPG_DIR/blob_parse $UPG_DIR/libupgrade.so rsync > /dev/null
   rm -rf $UPG_DIR/libcgos.so $UPG_DIR/cgutlcmd $UPG_DIR/cgosdrv.ko > /dev/null
   rm -rf $UPG_DIR/uart_isp > /dev/null
   rm -rf $UPG_DIR/serial_menu > /dev/null
   rm -rf $UPG_DIR/get_version > /dev/null
   rm -rf $UPG_DIR/led_blink > /dev/null
   rm -rf $UPG_DIR/timelimit > /dev/null
   rm -rf $UPG_DIR/*.service > /dev/null
   rm -rf $UPG_DIR/calibration > /dev/null
   rm -rf fstab > /dev/null
   rm -fr $UPG_DIR/.upg_avail > /dev/null
}

if [ ! -f $UPG_DIR/blob_parse ]; then
   logger "[sbc_upgrade]: Cannot find upgrade blob parse"
   clean_files
   blink_led $red_led 0 20
   exit 1
fi

if [ ! -f $UPG_DIR/blob.bin ]; then
   logger "[sbc_upgrade]: Cannot find upgrade blob"
   clean_files
   blink_led $red_led 0 20
   exit 1
fi

if [ ! -f $UPG_DIR/libupgrade.so ]; then
   logger "[sbc_upgrade]: Cannot find upgrade library"
   clean_files
   blink_led $red_led 0 20
   exit 1
fi

blink_led $green_led 0 10
if [ -f $UPG_DIR/blob.bin ]; then
   logger "[sbc_upgrade]: Parsing blob ..."
   nice -n -20 $UPG_DIR/blob_parse -i $UPG_DIR/blob.bin -o $UPG_DIR/
   if [ $? != 0 ]; then
      logger "[sbc_upgrade]: Blob parse not successful"
      clean_files
      blink_led $red_led 0 20
      exit 1
   fi

   # Check for output files
   if [ ! -f $UPG_DIR/lk.bin ]; then
      logger "[sbc_upgrade]: Blob parse lk.bin is missing!"
      clean_files
      blink_led $red_led 0 20
      exit 1
   fi
   
   if [ ! -f $UPG_DIR/rfs.bin ]; then
      logger "[sbc_upgrade]: Blob parse rfs.bin is missing!"
      clean_files
      blink_led $red_led 0 20
      exit 1
   fi

   #if [ ! -f $UPG_DIR/sdk.bin ]; then
   #   logger "[sbc_upgrade]: Blob parse: NOT UPGRADING: SDK!"
      #clean_files
      #exit 1
   #fi

   # Parsing is successful, we can remove it.
   rm -rf $UPG_DIR/blob.bin
else
   logger "[sbc_upgrade]: No upgrade bin found, quitting ..."
fi

blink_led $green_led 0 10
# Store the Linux kernel into the appropriate location
if [ -f $UPG_DIR/lk.bin ]; then
   if [ $ACTIVE_BANK = 0 ]; then
      logger "[sbc_upgrade]: Upgrading Linux kernel 1 ..."
      cp -v $UPG_DIR/lk.bin /boot/bzImage1
   else
      logger "[sbc_upgrade]: Upgrading Linux kernel 0 ..."
      cp -v $UPG_DIR/lk.bin /boot/bzImage0
   fi
fi

blink_led $green_led 0 10
# Check to see if the calibration folder exists, if it exists we
# want to preserve the calibration folder.
mkdir -p $UPG_DIR/calibration
if [ -d /home/root/sense/calibration ]; then
   logger "[sbc_upgrade]: Saving calibration folder ..."
   cp -a /home/root/sense/calibration/*.npy $UPG_DIR/calibration/
else
   mkdir -p $UPG_DIR/calibration
   cp -a /home/root/sense/*.npy $UPG_DIR/calibration/
fi

blink_led $green_led 0 10
logger "[sbc_upgrade]: Unpacking upgrade, please wait ..."
if [ -f $UPG_DIR/rfs.bin ]; then
   #mv $UPG_DIR/rfs.bin $UPG_DIR/rootfs.tar.bz2
   nice -n -20 tar -xvf $UPG_DIR/rfs.bin --warning=no-timestamp -C $UPG_DIR > /dev/null 2>&1
   if [ $? != 0 ]; then
      logger "[sbc_upgrade]: Corrupt ROOTFS during extracting ..."
      logger "[sbc_upgrade]: Stopping upgrade "
      clean_files
      exit 1
   fi

   cd $UPG_DIR
   #mkdir -p tmp_rootfs
   #mount rootfs tmp_rootfs
   if [ $ACTIVE_BANK = 0 ]; then
      # We are upgrade to bank 1
      blink_led $green_led 0 10
      logger "[sbc_upgrade]: Upgrading RFS bank 1 ..."
      nice -n -20 e2fsck -y /dev/sda3
      mkdir -p bank1
      mount /dev/sda3 bank1
      cp -a bank1/etc/fstab .
      umount bank1
      blink_led $green_led 0 10
      nice -n -20 dd if=rootfs of=/dev/sda3 bs=64K
      blink_led $green_led 0 10
      mount /dev/sda3 bank1
      cp -a fstab bank1/etc/

      cp -a $UPG_DIR/sensesdk-mgr.service bank1/lib/systemd/system/
      cp -a $UPG_DIR/sense1_mgr.service bank1/lib/systemd/system/
      cp -a $UPG_DIR/sbc_upgrade.service bank1/lib/systemd/system/

      cp -a $UPG_DIR/cleanup_upgrade.service bank1/lib/systemd/system/
      rm -fr bank1/etc/lib/systemd/system/multi-user.target.wants/cleanup_upgrade.service
      pushd .
      cd bank1/etc/lib/systemd/system/multi-user.target.wants
      ln -s /lib/systemd/system/multi-user.target.wants/cleanup_upgrade.service
      popd

      #logger "[sbc_upgrade]: Updating the SDK service option ..."
      #if [ -l bank1/etc/systemd/system/multi-user.target.wants/sensesdk-mgr.service ]; then
         # Need to point to the new service file for sensesdk-mgr
         #rm -fr bank1/etc/systemd/system/multi-user.target.wants/sensesdk-mgr.service
         #pushd .
         #cd bank1/etc/systemd/system/multi-user.target.wants
         #ln -s /lib/systemd/system/sensesdk-mgr.service
         #popd
      #fi


      # Restore the calibration directory rather than the generic directory.
      if [ -d bank1/home/root/sense/calibration ]; then
         cp -a $UPG_DIR/calibration/*.npy bank1/home/root/sense/calibration/
      fi

      ROOTFS1_UUID=`cat /boot/loader/entries/boot1.conf | grep PARTUUID | awk '{ print $2 }'`
      echo -e "title boot1\n
      linux /bzImage1\n
      options $ROOTFS1_UUID rw rootwait quiet 3 snd_hda_intel.power_save=1 snd_hda_intel.power_save_controller=y scsi_mod.scan=async i915.enable_guc_loading=1 reboot=efi console=ttyS2,115200n8 rootwait console=ttyS0,115200 console=tty0 fsck.mode=force fsck.repair=yes" > boot1.conf
      mv boot1.conf /boot/loader/entries/

      echo -e "default boot1\ntimeout 3 " > loader.conf
      mv loader.conf /boot/loader/
      umount bank1 > /dev/null 2>&1
      rm -fr bank1
      nice -n -20 e2fsck -y /dev/sda3
      # On the next reboot
      ACTIVE_BANK=1
      echo "$ACTIVE_BANK" > $UPG_DIR/active_bank
      logger "[sbc_upgrade]: Upgrading RFS bank 1: successful"
   elif [ $ACTIVE_BANK = 1 ]; then
      logger "[sbc_upgrade]: Upgrading RFS bank 0 ..."
      mkdir -p bank0
      nice -n -20 e2fsck -y /dev/sda2
      mount /dev/sda2 bank0
      cp -a bank0/etc/fstab .
      umount bank0
      blink_led $green_led 0 10
      nice -n -20 dd if=rootfs of=/dev/sda2 bs=64K
      blink_led $green_led 0 10
      mount /dev/sda2 bank0
      cp -a fstab bank0/etc/

      cp -a $UPG_DIR/sensesdk-mgr.service bank0/lib/systemd/system/
      cp -a $UPG_DIR/sense1_mgr.service bank0/lib/systemd/system/
      cp -a $UPG_DIR/sbc_upgrade.service bank0/lib/systemd/system/

      cp -a $UPG_DIR/cleanup_upgrade.service bank0/lib/systemd/system/
      rm -fr bank0/etc/lib/systemd/system/multi-user.target.wants/cleanup_upgrade.service
      pushd .
      cd bank0/etc/lib/systemd/system/multi-user.target.wants
      ln -s /lib/systemd/system/multi-user.target.wants/cleanup_upgrade.service
      popd

      # Need to update the SDK service to non-demo option
      #logger "[sbc_upgrade]: Updating the SDK service option ..."
      #if [ -L bank0/etc/systemd/system/multi-user.target.wants/sensesdk-mgr.service ]; then
         # Need to point to the new service file for sensesdk-mgr
         #rm -fr bank0/etc/systemd/system/multi-user.target.wants/sensesdk-mgr.service
         #pushd .
         #cd bank0/etc/systemd/system/multi-user.target.wants
         #ln -s /lib/systemd/system/sensesdk_mgr.service
         #popd
      #fi

      # Restore the calibration directory rather than the generic directory.
      logger "[sbc_upgrade]: Restoring the calibration files ..."
      if [ -d bank0/home/root/sense/calibration ]; then
         cp -a $UPG_DIR/calibration/*.npy bank0/home/root/sense/calibration/
      fi

      ROOTFS0_UUID=`cat /boot/loader/entries/boot0.conf | grep PARTUUID | awk '{ print $2 }'`
      echo -e "title boot0\n
      linux /bzImage0\n
      options $ROOTFS0_UUID rw rootwait quiet 3 snd_hda_intel.power_save=1 snd_hda_intel.power_save_controller=y scsi_mod.scan=async i915.enable_guc_loading=1 reboot=efi console=ttyS2,115200n8 rootwait console=ttyS0,115200 console=tty0 fsck.mode=force fsck.repair=yes" > boot0.conf
      mv boot0.conf /boot/loader/entries/

      echo -e "default boot0\ntimeout 3 " > loader.conf
      mv loader.conf /boot/loader/
      umount bank0 > /dev/null 2>&1
      rm -fr bank0
      nice -n -20 e2fsck -y /dev/sda2
      # On the next reboot
      ACTIVE_BANK=0
      echo "$ACTIVE_BANK" > $UPG_DIR/active_bank
      logger "[sbc_upgrade]: Upgrading RFS bank 0: successful"
   else
      logger "[sbc_upgrade]: ACTIVE_BANK has an error"
      clean_files
      exit 1
   fi
   umount tmp_rootfs
   rm -fr tmp_rootfs
   #reboot 
fi

# At this point, the root filesystem is upgraded, we can remove the ugprade trigger.
logger "[sbc_upgrade]: Removing upgrade trigger"
rm -fr $UPG_DIR/.upg_avail

logger "[sbc_upgrade]: Installing tools ..."
if [ ! -d /data/tools ]; then
   logger "[sbc_upgrade]: Creating tools directory"
   mkdir -p /data/tools
   if [ $? != 0 ]; then
      logger "[sbc_upgrade]: FAILED to create a tools directory"
   fi
else
   logger "[sbc_upgrade]: Tools directory exists"
fi

DATA_DIR_ERROR=0

cp -v $UPG_DIR/led_blink /data/tools/
   if [ $? != 0 ]; then
      DATA_DIR_ERROR=1
      logger "[sbc_upgrade]: FAILED to copy led_blink to /data/tools directory"
   fi
cp -v $UPG_DIR/reset.bin /data/tools/
   if [ $? != 0 ]; then
      DATA_DIR_ERROR=1
      logger "[sbc_upgrade]: FAILED to copy reset.bin to /data/tools directory"
   fi
cp -v $UPG_DIR/uart_isp  /data/tools/
   if [ $? != 0 ]; then
      DATA_DIR_ERROR=1
      logger "[sbc_upgrade]: FAILED to copy uart_isp to /data/tools directory"
   fi
cp -v $UPG_DIR/serial_menu /data/tools/
   if [ $? != 0 ]; then
      DATA_DIR_ERROR=1
      logger "[sbc_upgrade]: FAILED to copy serial_menu to /data/tools directory"
   fi
cp -v $UPG_DIR/get_version /data/tools/
   if [ $? != 0 ]; then
      DATA_DIR_ERROR=1
      logger "[sbc_upgrade]: FAILED to copy get_version to /data/tools directory"
   fi
cp -v $UPG_DIR/get_board_rev /data/tools/
   if [ $? != 0 ]; then
      DATA_DIR_ERROR=1
      logger "[sbc_upgrade]: FAILED to copy get_board_rev to /data/tools directory"
   fi
cp -v $UPG_DIR/pwr_brd_info /data/tools/
   if [ $? != 0 ]; then
      DATA_DIR_ERROR=1
      logger "[sbc_upgrade]: FAILED to copy pwr_brd_info to /data/tools directory"
   fi
cp -v $UPG_DIR/restore.sh /data/tools/
   if [ $? != 0 ]; then
      DATA_DIR_ERROR=1
      logger "[sbc_upgrade]: FAILED to copy restore.sh to /data/tools directory"
   fi

cp -v $UPG_DIR/cleanup_upgrade.sh /data/tools/
   if [ $? != 0 ]; then
      DATA_DIR_ERROR=1
      logger "[sbc_upgrade]: FAILED to copy cleanup_upgrade.sh to /data/tools directory"
   fi

if [ $DATA_DIR_ERROR != 0 ]; then
      logger "[sbc_upgrade]: Something is wrong with /etc/fstab - usually wrong partition UUID specified"
      logger "[sbc_upgrade]: Stopping upgrade - contact for technical help"
      clean_files
      exit 1;
fi

logger "[sbc_upgrade]: Creating tools directory completed"


logger "[sbc_upgrade]: Trying to get the Power Board version ..."

PW_VERSION=0.0.0

./$UPG_DIR/timelimit -t 5 $UPG_DIR/get_version
GET_VERSION_RESULT=$?
if [ $GET_VERSION_RESULT != 127 ]; then
   logger "[sbc_upgrade]: Power Board version: successful"
   PW_VERSION=`$UPG_DIR/get_version`
else
   logger "[sbc_upgrade]: Power Board version: Timeout,  default 0.0.0"   
fi

logger "[sbc_upgrade]: Power Board VERSION: $PW_VERSION"

power_fw_error=0
blink_led $green_led 0 10
if [ -f $UPG_DIR/power_board.bin ]; then
   logger "[sbc_upgrade]: Trying to upgrade the Power Board Firmware ..."
   nice -n -20 $UPG_DIR/uart_isp -i $UPG_DIR/power_board.bin -r $UPG_DIR/reset.bin
   ERROR_CODE=$?
   if [ $ERROR_CODE = 0 ]; then
      logger "[sbc_upgrade]: Power Board FW Upgrade: successful"
   else
      logger "[sbc_upgrade]: Power Board Firmware Upgrade: failed with error = $ERROR_CODE"
      logger "[sbc_upgrade]: Power Board has old firmware, which cannot be upgraded"
      logger "[sbc_upgrade]: Please power cycle unit"
      # blink_led $red_led 1 1
      echo $ERROR_CODE > $UPG_DIR/error_code.txt
      power_fw_error=1
   fi
fi

if [ $power_fw_error = 0 ]; then
   NEW_PW_VERSION=0.0.0

   ./$UPG_DIR/timelimit -t 5 $UPG_DIR/get_version

   if [ $? != 143 ]; then
      NEW_PW_VERSION=`$UPG_DIR/get_version`
   fi

   logger "[sbc_upgrade]: New Power Board VERSION: $NEW_PW_VERSION"
fi

#logger "[sbc_upgrade]: Upgrading SDK - not supported yet"
#if [ -f $UPG_DIR/sdk.bin ]; then
#   logger "[sbc_upgrade]: Not supported yet -TBD"
#fi

sync
logger "[sbc_upgrade]: --------------------------------------------------------"
logger "[sbc_upgrade]: Complete with BANK$ACTIVE_BANK upgrade!"
logger "[sbc_upgrade]: Power cycle unit after unit reboot for all devices to be initialized properly"
logger "[sbc_upgrade]: NOTE: If successful, green led will blink 25 times"
logger "[sbc_upgrade]: NOTE: If red led is lit, error in upgrade or backward compatibility issue"
logger "[sbc_upgrade]: --------------------------------------------------------"

logger "[sbc_upgrade]: Rebooting in 5 seconds!"
blink_led $green_led 0 26
#sleep 5s

clean_files


reboot

