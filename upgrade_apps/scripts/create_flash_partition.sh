#!/bin/bash

function usage()
{
   printf "usage: ./create_flash_partition.sh ROOTFS=<rootfs_file.EXT4> <device>\n\n"
   exit 1
}

if [ "$USER" != "root" ]; then
   printf "You must be root - sudo ./create_flash_partition\n"
   exit 1
fi


if [[ "$1" == "ROOTFS"* ]]; then
   ROOTFS_IMAGE=`echo $1 | sed 's/ROOTFS=//g'`
fi

if [[ "$2" == "ROOTFS"* ]]; then
   ROOTFS_IMAGE=`echo $2 | sed 's/ROOTFS=//g'`
fi

if [ "$ROOTFS_IMAGE" = "" ]; then
   usage
fi

if [[ "$1" == "/dev/sd"* ]]; then
   DEVICE=$1
fi

if [[ "$2" == "/dev/sd"* ]]; then
   DEVICE=$2
fi

if [ "$ROOTFS_IMAGE" = "" ]; then
   usage
fi

printf "ROOTFS = $ROOTFS_IMAGE\n"

# Please edit to point to the boot EFI as part of the creating the flash partitions.
# It can be the absolute path. (Can be a symbolic link to images from Yocto build)
BOOT_IMAGE=bootx64.efi

# Please edit to point to the Linux kernel as part of the creating of the flash paritions
# It can be the absolute path. (Can be a symbolic link to images from Yocto build)
LINUX_IMAGE=bzImage


if [ ! -f $BOOT_IMAGE ]; then
   printf "Please point to a valid Boot EFI image, i.e., bootx64.efi\n"
   exit 1
fi

if [ ! -f $LINUX_IMAGE ]; then
   printf "Please point to a valid Linux Kernel image, i.e., bzImage\n"
   exit 1
fi

if [ ! -f $ROOTFS_IMAGE ]; then
   printf "Please point to a valid Root Filesystem image, i.e., xxxx*rootfs.ext4\n"
   exit 1
fi

EFI_DEVICE=$DEVICE"1"
ROOTFS0_DEVICE=$DEVICE"2"
ROOTFS1_DEVICE=$DEVICE"3"
DATA_DEVICE=$DEVICE"4"
SWAP_DEVICE=$DEVICE"5"



# Zero out the memory device and generating a GPT table.
sgdisk -zg $DEVICE
if [ $? != 0 ]; then
   printf "sgdisk error: Zero-ing out [%s]\n" $DEVICE
   exit 1
fi

# Determine the size of the FLASH
ENDSECTOR=`sgdisk -E $DEVICE`
if [ $ENDSECTOR = 62533262 ]; then
   printf "Flash size detected: 32 GB\n"
fi

if [ $ENDSECTOR = 117231374 ]; then
   printf "Flash size detected: 60 GB\n"
fi


# Creating the EFI partition (31 MB).
sgdisk -n 1:2048:65535 -c 1:"EFI System" -t 1:ef00 $DEVICE
if [ $? != 0 ]; then
   printf "sgdisk error: Creating the EFI partition (%s)\n" $EFI_DEVICE
   exit 1
fi

# Creating the ROOTFS bank 0 (6GB).
sgdisk -n 2:65536:10551295 -c 2:"rootfs0" -t 2:8300 $DEVICE
if [ $? != 0 ]; then
   printf "sgdisk error: Creating the ROOTFS bank 0 partition (%s)\n" $ROOTFS0_DEVICE
   exit 1
fi

# Creating the ROOTFS bank 1 (6GB).
sgdisk -n 3:10551296:21102591 -c 3:"rootfs1" -t 3:8300 $DEVICE
if [ $? != 0 ]; then
   printf "sgdisk error: Creating the ROOTFS bank 1 partition (%s)\n" $ROOTFS1_DEVICE
   exit 1
fi

# Creating the DATA paritition (15GB).
sgdisk -n 4:21102592:58851327 -c 4:"data" -t 4:8300 $DEVICE
if [ $? != 0 ]; then
   printf "sgdisk error: Creating the DATA partition (%s)\n" $DATA_DEVICE
   exit 1
fi

# Creating the SWAP partition.
ENDSECTOR=`sgdisk -E $DEVICE`
sgdisk -n 5:58851328:$ENDSECTOR -c 5:"swap" -t 5:8200 $DEVICE
if [ $? != 0 ]; then
   printf "sgdisk error: Creating the SWAP partition (%s)\n" $SWAP_DEVICE
   exit 1
fi

sgdisk -p $DEVICE

sgdisk -v $DEVICE

partprobe $DEVICE

sleep 5s

BOOT_UUID=`sgdisk --info=1 $DEVICE | grep "unique" |  awk '{print tolower($4)}'`
ROOTFS0_UUID=`sgdisk --info=2 $DEVICE | grep "unique" |  awk '{print tolower($4)}'`
ROOTFS1_UUID=`sgdisk --info=3 $DEVICE | grep "unique" |  awk '{print tolower($4)}'`
DATA_UUID=`sgdisk --info=4 $DEVICE | grep "unique" |  awk '{print tolower($4)}'`
SWAP_UUID=`sgdisk --info=5 $DEVICE | grep "unique" |  awk '{print tolower($4)}'`


printf "ROOTFS0 UUID = %s\n" $ROOTFS0_UUID
printf "ROOTFS1 UUID = %s\n" $ROOTFS1_UUID
printf "DATA UUID = %s\n" $DATA_UUID
printf "SWAP_UUID = %s\n" $SWAP_UUID

# Generate boot.conf - read the partition UUID to store for ROOTFS
echo -e "title boot0\n
linux /bzImage0\n
options root=PARTUUID=$ROOTFS0_UUID rw rootwait quiet 3 snd_hda_intel.power_save=1 snd_hda_intel.power_save_controller=y scsi_mod.scan=async i915.enable_guc_loading=1 reboot=efi console=ttyS2,115200n8 rootwait console=ttyS0,115200 console=tty0 fsck.mode=force fsck.repair=yes" > boot0.conf

echo -e "title boot1\n
linux /bzImage1
options root=PARTUUID=$ROOTFS1_UUID rw rootwait quiet 3 snd_hda_intel.power_save=1 snd_hda_intel.power_save_controller=y scsi_mod.scan=async i915.enable_guc_loading=1 reboot=efi console=ttyS2,115200n8 rootwait console=ttyS0,115200 console=tty0 fsck.mode=force fsck.repair=yes" > boot1.conf


# Generate fstab relevant to the ROOTFS partitions
echo -e "/dev/root            /                    auto       defaults              1  1
proc                 /proc                proc       defaults              0  0
devpts               /dev/pts             devpts     mode=0620,gid=5       0  0
tmpfs                /run                 tmpfs      mode=0755,nodev,nosuid,strictatime 0  0
tmpfs                /var/volatile        tmpfs      defaults              0  0

/dev/disk/by-partuuid/$SWAP_UUID  none             swap       defaults              0  0
/dev/disk/by-partuuid/$BOOT_UUID  /boot            vfat       defaults              1  2
/dev/disk/by-partuuid/$DATA_UUID  /data            ext4       defaults              1  2 " > fstab

echo -e "default boot0\ntimeout 3 " > loader.conf

printf "Populating flash ...\n"

mkdir -p efi
umount $EFI_DEVICE > /dev/null

mkfs.vfat $EFI_DEVICE

mount $EFI_DEVICE efi
if [ $? != 0 ]; then
   printf "mount error: Cannot mount EFI partition (%s)\n" $EFI_DEVICE
   exit 1
fi

rm -fr efi/*

mkdir -p efi/EFI
mkdir -p efi/EFI/BOOT
mkdir -p efi/loader
mkdir -p efi/loader/entries

cp -v $LINUX_IMAGE efi/bzImage0
cp -v $LINUX_IMAGE efi/bzImage1

cp -v bootx64.efi efi/EFI/BOOT

cp -v boot*.conf efi/loader/entries/
cp -v loader.conf efi/loader/

umount efi
sleep 1s
rm -fr efi
printf "Finalized EFI partition\n"

printf "Mounting the SOURCE ROOTFS to be replicated on the new ROOTFS(es)\n"
if [ -d rootfs ]; then
   umount rootfs
fi

mkdir -p rootfs
mount $ROOTFS_IMAGE rootfs
if [ $? != 0 ]; then
   printf "mount error: Cannot mount the ROOTFS: [%s]\n" $ROOTFS_IMAGE
   exit 1
fi

printf "========================================================\n"
printf " Creating ROOTFS bank 0 partition (%s)           \n" $ROOTFS0_DEVICE
printf "========================================================\n"

if [ -d rootfs0 ]; then
   umount rootfs0
fi
mkdir -p rootfs0
umount $ROOTFS0_DEVICE > /dev/null

mkfs.ext4 -F $ROOTFS0_DEVICE
mount $ROOTFS0_DEVICE rootfs0
if [ $? != 0 ]; then
   printf "mount error: Cannot mount the ROOTFS bank 0 partition (%s)\n" $ROOTFS0_DEVICE
   umount rootfs
   exit 1
fi

rm -fr rootfs0/*

printf "Transferring rootfs to [%s] ..\n" $ROOTFS0_DEVICE
rsync -a --exclude=.git/ --info=progress2 rootfs/* rootfs0/
if [ $? != 0 ]; then
   printf "rsync error: Cannot copy the ROOTFS bank 0 partition (%s)\n" $ROOTFS0_DEVICE
   umount rootfs
   umount rootfs0
   exit 1
fi

printf "Updating File system table: rootfs0/etc/fstab\n"
cp -v fstab rootfs0/etc/

cp -v sbc_upgrade.service rootfs0/lib/systemd/system/
cp -v sensesdk-mgr.service rootfs0/lib/systemd/system/
cp -v sense1_mgr.service rootfs0/lib/systemd/system/
cp -v cleanup_upgrade.service rootfs0/lib/systemd/system/

pushd .
cd rootfs0/etc/systemd/system/multi-user.target.wants
if [ ! -L sbc_upgrade.service ]; then 
   ln -s /lib/systemd/system/sbc_upgrade.service
fi

if [ ! -L sensesdk-mgr.service ]; then
   ln -s /lib/systemd/system/sensesdk-mgr.service
fi

if [ ! -L cleanup_upgrade.service ]; then
   ln -s /lib/systemd/system/cleanup_upgrade.service
fi

popd

sync
printf "Finalizing the ROOTFS bank 0 partition (%s) ...\n" $ROOTFS0_DEVICE
umount rootfs0 > /dev/null
sleep 1s
rm -fr rootfs0

printf "Complete creating ROOTFS bank 0 (%s) !\n" $ROOTFS0_DEVICE

printf "========================================================\n"
printf " Creating ROOTFS bank 1 partition (%s)           \n" $ROOTFS1_DEVICE
printf "========================================================\n"

if [ -d rootfs1 ]; then
   umount rootfs1
fi

mkdir -p rootfs1
umount $ROOTFS1_DEVICE > /dev/null

mkfs.ext4 -F $ROOTFS1_DEVICE
mount $ROOTFS1_DEVICE rootfs1
if [ $? != 0 ]; then
   printf "mount error: Cannot mount the ROOTFS bank 1 partition (%s)\n" $ROOTFS1_DEVICE
   umount rootfs
   exit 1
fi

rm -fr rootfs1/*

printf "Initially, bank 1 is duplicated of bank 0 ..\n"
printf "Transferring rootfs to [%s] ..\n" $ROOTFS1_DEVICE
rsync -a --exclude=.git/ --info=progress2 rootfs/* rootfs1/
if [ $? != 0 ]; then
   printf "rsync error: Cannot copy the ROOTFS bank 1 partition (%s)\n" $ROOTFS1_DEVICE
   umount rootfs
   umount rootfs1
   exit 1
fi

printf "Updating File system table: rootfs1/etc/fstab\n"
cp -a fstab rootfs1/etc/

cp -v sbc_upgrade.service rootfs1/lib/systemd/system/
cp -v sensesdk-mgr.service rootfs1/lib/systemd/system/
cp -v sense1_mgr.service rootfs1/lib/systemd/system/
cp -v cleanup_upgrade.service rootfs1/lib/systemd/system/

pushd .
cd rootfs1/etc/systemd/system/multi-user.target.wants
if [ ! -L sbc_upgrade.service ]; then 
   ln -s /lib/systemd/system/sbc_upgrade.service
fi

if [ ! -L sensesdk-mgr.service ]; then
   ln -s /lib/systemd/system/sensesdk-mgr.service
fi

if [ ! -L cleanup_upgrade.service ]; then
   ln -s /lib/systemd/system/cleanup_upgrade.service
fi
popd

sync
printf "Finalizing the ROOTFS bank 1 partition (%s) ...\n" $ROOTFS1_DEVICE
umount rootfs1
sleep 1s
rm -fr rootfs1
printf "Complete creating ROOTFS bank 1 (%s) !\n" $ROOTFS1_DEVICE
umount rootfs

printf "========================================================\n"
printf " Creating DATA partition (%s)                    \n" $DATA_DEVICE
printf "========================================================\n"

mkdir -p data_dir

mkfs.ext4 -F $DATA_DEVICE

mount -v $DATA_DEVICE data_dir
if [ $? != 0 ]; then
   printf "mount error: Cannot mount the DATA partition (%s)\n" $DATA_DEVICE
   umount data_dir
   rm -fr data_dir
   exit 1
fi

mkdir -p data_dir/.upgrade
cp -v sbc_upgrade data_dir/.upgrade/
echo "0" > data_dir/.upgrade/active_bank

mkdir -p data_dir/tools
cp -v uart_isp data_dir/tools/
cp -v led_blink data_dir/tools/
cp -v reset.bin data_dir/tools/
cp -v serial_menu data_dir/tools/
cp -v get_version data_dir/tools/
cp -v get_board_rev data_dir/tools/
cp -v pwr_brd_info data_dir/tools/
cp -v restore.sh data_dir/tools/
cp -v cleanup_upgrade.sh data_dir/tools/

sync
echo "Waiting for 2 sec"
sleep 2s

umount -v data_dir
if [ $? != 0 ]; then
   printf "umount error: Cannot umount DATA partition (%s)\n" $DATA_DEVICE
   exit 1
fi

sleep 1s
printf "Complete creating the DATA partition (%s) !\n" $DATA_DEVICE

sync

printf "========================================================\n"
printf " Creating SWAP partition (%s)                    \n" $SWAP_DEVICE
printf "========================================================\n"

mkswap $SWAP_DEVICE

rm -fr rootfs rootfs0 rootfs1 data_dir boot0.conf boot1.conf loader.conf fstab

printf "Complete creating image on FLASH!\n"

