#!/bin/bash

INSTALLER_VERSION=1.0.4

function usage()
{
   printf "usage: ./installer-$INSTALLER_VERSION.sh <option>-\n"
   printf "option:\n"
   printf "    -i=<upgrade_file>          - File to upgrade, i.e.rootfs0.ext4.bin\n\n"
   printf "    -ip=<ip address>           - IP Address of the SBC device\n\n"
   exit 1
}

if [[ "$1" == "-i="* ]]; then
   UPGRADE_FILE=`echo $1 | sed 's/-i=//g'`
fi

if [[ "$2" == "-i="* ]]; then
   UPGRADE_FILE=`echo $2 | sed 's/-i=//g'`
fi

if [ "$UPGRADE_FILE" = "" ]; then
   UPGRADE_FILE=`ls *.bz2`
fi

if [[ "$1" == "-ip="* ]]; then
   IP_ADDRESS=`echo $1 | sed 's/-ip=//g'`
fi

if [[ "$2" == "-ip="* ]]; then
   IP_ADDRESS=`echo $2 | sed 's/-ip=//g'`
fi

if [ "$IP_ADDRESS" = "" ]; then
   printf "Using default IP 192.168.1.20\n"
   IP_ADDRESS=192.168.1.20
fi

printf "PLEASE SAVE ALL DATA FROM UNIT /data will be cleaned\n"
printf "Continue with upgrade? (y/n) "
read response

if [ "$response" != "y" ]; then
   printf "Stopping upgrade ...\n"
   exit 1
fi


#DATA_DIR_LIST=`ssh root@$IP_ADDRESS ls -w 0 /data`

#for i in $DATA_DIR_LIST
#do
#	if [ "$i" != ".upgrade" ] && [ "$i" != "tools" ]; then
#		printf "Cleaning SBC file %s\n" $i
#		#ssh root@$IP_ADDRESS ls -w 0 /data/.upgrade
#		ssh root@$IP_ADDRESS rm -rf /data/$i
#	fi
#done


LIST=`ssh root@$IP_ADDRESS ls -w 0 /data/.upgrade`

for i in $LIST
do
	if [ "$i" != "sbc_upgrade" ] && [ "$i" != "active_bank" ]; then
		printf "Cleaning SBC file %s\n" $i
		#ssh root@$IP_ADDRESS ls -w 0 /data/.upgrade
		ssh root@$IP_ADDRESS rm -rf /data/.upgrade/$i
	fi
done

# Add sanity check for existence of upgrade file
if [ ! -f $UPGRADE_FILE ]; then
   printf "File %s doesn't exist!\n" $UPGRADE_FILE
   exit 1
fi

printf "Installer version: %s\n" $INSTALLER_VERSION
printf "File to upgrade SBC: %s\n" $UPGRADE_FILE
printf "Device IP: %s\n" $IP_ADDRESS

printf "Stopping sensesdk-mgr service ...\n"
ssh root@$IP_ADDRESS systemctl stop sensesdk-mgr

UPGRADE_PROCESS=`ssh root@$IP_ADDRESS systemctl is-active sbc_upgrade`
if [ "$UPGRADE_PROCESS" != "active" ]; then
   printf "SBC_UPGRADE is not active, starting SBC_UPGRADE service ...\n"
   ssh root@$IP_ADDRESS systemctl start sbc_upgrade
else
   printf "SBC_UPGRADE service is active\n"
fi

scp $UPGRADE_FILE root@$IP_ADDRESS:/data/.upgrade/
ERROR_CODE=$?
if [ $ERROR_CODE != 0 ]; then
   printf "Initiate upgrade failed.  (%s) Error code %d\n" $UPGRADE_FILE $ERROR_CODE
   exit 1
fi

echo "$UPGRADE_FILE" > .upg_avail
scp .upg_avail root@$IP_ADDRESS:/data/.upgrade/
ERROR_CODE=$?
if [ $ERROR_CODE != 0 ]; then
   printf "Initiate upgrade failed.  (.upg_avail) Error code %d\n" $ERROR_CODE
   exit 1
fi
ssh root@$IP_ADDRESS tail -f /var/log/messages | grep "sbc_upgrade"

rm -fr .upg_avail





