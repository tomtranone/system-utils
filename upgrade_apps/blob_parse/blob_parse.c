#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>

#include "fw_blob.h"

int main(int argc, char **argv)
{
   char *input = NULL;
   char *output_dir = NULL;
   char c;


   while((c = getopt (argc, argv, "i:o:")) != -1) {
      
      switch (c) {
         case 'i':
            input = optarg;
            break;
         case 'o':
            output_dir = optarg;
            break;
         case '?':
            if(optopt == 'i') {
               printf("Option -%c requires an argument.\n", optopt);
            } else if (isprint (optopt)) {
               printf("Unknown option `-%c'.\n", optopt);
            } else {
               printf("Unknown option character `\\x%x'.\n",optopt);
            }
            exit(1);

         default:
            exit(1);
      }
   }

   return blob_parse(input, output_dir);
}
